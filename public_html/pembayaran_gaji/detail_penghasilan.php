<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Penggajian
			<small>Detail Penghasilan</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?= base_url() ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
			<li><a href="<?= base_url('pembayaran_gaji') ?>"> Pembayaran Gaji</a></li>
			<li><a href="<?= base_url('pembayaran_gaji/daftar_penerima_gaji/' . $pembayaran_gaji['id']) ?>"> Daftar Penerima Gaji</a></li>
			<li class="active">Pembayaran Gaji Baru</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<?php if($this->session->flashdata('msg_g')) : ?>
					<div class="alert alert-danger alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<h5><i class="icon fa fa-ban"></i> <?= $this->session->flashdata('msg_g') ?></h5>
					</div>
				<?php elseif($this->session->flashdata('msg')) : ?>
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<h5><i class="icon fa fa-check"></i> <?= $this->session->flashdata('msg') ?></h5>
					</div>
				<?php endif; ?>
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title"><?= $penerima_gaji['nama_depan'] . ' ' . $penerima_gaji['nama_belakang'] ?></h3>
					</div>
					<form class="form-horizontal" enctype="multipart/form-data" action="<?= base_url('pembayaran_gaji/do_edit_penghasilan') ?>" method="POST">
						<div class="box-body">
							<div class="form-group">
								<div class="col-sm-2">
									<label>NRP</label>
								</div>
								<div class="col-sm-4">
									<span>: <?= $penerima_gaji['nrp'] ?></span>
								</div>
								<div class="col-sm-2">
									<label>Nama</label>
								</div>
								<div class="col-sm-4">
									<span>: <?= $penerima_gaji['nama_depan'] . ' ' . $penerima_gaji['nama_belakang'] ?></span>
								</div>
							</div>
							<div class="form-group"> 
								<div class="col-sm-2">
									<label>Pangkat</label>
								</div>
								<div class="col-sm-4">
									<span>: <?= $penerima_gaji['nama_pangkat'] ?></span>
								</div>
								<div class="col-sm-2">
									<label>Jabatan</label>
								</div>
								<div class="col-sm-4">
									<span>: <?= $penerima_gaji['nama_jabatan'] ?></span>
								</div>
							</div>
							<div class="form-group"> 
								<div class="col-sm-2">
									<label>Kode Pembayaran</label>
								</div>
								<div class="col-sm-4">
									<span>: <?= $pembayaran_gaji['kode_bayar_gaji'] ?></span>
								</div>
								<div class="col-sm-2">
									<label>Periode</label>
								</div>
								<div class="col-sm-4">
									<span>: <?= date("d M Y", $pembayaran_gaji['periode_gaji_dari']) . ' s/d ' . date("d M Y", $pembayaran_gaji['periode_gaji_sampai'])?></span>
								</div>
							</div>
							<hr>
							<div class="form-group"> 
								<div class="col-sm-2">
									<label>Gaji Pokok</label>
								</div>
								<div class="col-sm-2">
									<span>: Rp. <?= number_format($penerima_gaji['gaji_pokok'], 0,',', '.') ?></span>
								</div>
								<div class="col-sm-2">
									<label>Tunjangan Suami/Istri</label>
								</div>
								<div class="col-sm-2">
									<span>: Rp. <?= number_format($penerima_gaji['tunjangan_suami_istri'], 0,',', '.') ?></span>
								</div>
								<div class="col-sm-2">
									<label>Tunjangan Anak</label>
								</div>
								<div class="col-sm-2">
									<span>: Rp. <?= number_format($penerima_gaji['tunjangan_anak'], 0,',', '.') ?></span>
								</div>
							</div>
							<div class="form-group"> 
								<div class="col-sm-2">
									<label>Gaji Bruto</label>
								</div>
								<div class="col-sm-2">
									<span>: Rp. <?= number_format($penerima_gaji['gaji_bruto'], 0,',', '.') ?></span>
								</div>
								<div class="col-sm-2">
									<label>Tunjangan Lauk Pauk</label>
								</div>
								<div class="col-sm-2">
									<span>: Rp. <?= number_format($penerima_gaji['tunjangan_lauk_pauk'], 0,',', '.') ?></span>
								</div>
								<div class="col-sm-2">
									<label>Tunjangan Umum</label>
								</div>
								<div class="col-sm-2">
									<span>: Rp. <?= number_format($penerima_gaji['tunjangan_umum'], 0,',', '.') ?></span>
								</div>
							</div>
							<div class="form-group"> 
								<div class="col-sm-2">
									<label>Tunjangan Beras</label>
								</div>
								<div class="col-sm-2">
									<span>: Rp. <?= number_format($penerima_gaji['tunjangan_beras'], 0,',', '.') ?></span>
								</div>
								<div class="col-sm-2">
									<label>Tunjangan Profesi</label>
								</div>
								<div class="col-sm-2">
									<span>: Rp. <?= number_format($penerima_gaji['tunjangan_profesi'], 0,',', '.') ?></span>
								</div>
								<div class="col-sm-2">
									<label>Jumlah Penghasilan Kotor</label>
								</div>
								<div class="col-sm-2">
									<span>: Rp. <?= number_format($penerima_gaji['jumlah_penghasilan_kotor'], 0,',', '.') ?></span>
								</div>
							</div>
							<hr>
							<div class="form-group"> 
								<div class="col-sm-2">
									<label>IWP</label>
								</div>
								<div class="col-sm-2">
									<span>: Rp. <?= number_format($penerima_gaji['iwp'], 0,',', '.') ?></span>
								</div>
								<div class="col-sm-2">
									<label>BPJS</label>
								</div>
								<div class="col-sm-2">
									<span>: Rp. <?= number_format($penerima_gaji['bpjs'], 0,',', '.') ?></span>
								</div>
								<div class="col-sm-2">
									<label>PPh Ps.21
										<!-- <button type="button" class="btn btn-secondary bulat" 
											data-toggle="tooltip" data-placement="top" 
											title="2% dari jumlah Jumlah Penghasilan Kotor, pembulatan ke atas">
											<i class="fa fa-fw fa-exclamation"></i>
										</button> -->
									</label>
								</div>
								<div class="col-sm-2">
									<span>: Rp. <?= number_format($penerima_gaji['pph_ps_21'], 0,',', '.') ?></span>
								</div>
							</div>
							<div class="form-group"> 
								<div class="col-sm-2">
									<label>Utang</label>
								</div>
								<div class="col-sm-2">
									<span>: Rp. <?= number_format($penerima_gaji['utang'], 0,',', '.') ?></span>
								</div>
								<div class="col-sm-2">
									<label>Jumlah Potongan</label>
								</div>
								<div class="col-sm-2">
									<span>: Rp. <?= number_format($penerima_gaji['jumlah_potongan'], 0,',', '.') ?></span>
								</div>
							</div>
							<div class="form-group"> 
								<div class="col-sm-2">
									<label>Jumlah Penghasilan Bersih</label>
								</div>
								<div class="col-sm-2">
									<span>: Rp. <?= number_format($penerima_gaji['jumlah_penghasilan_bersih'], 0,',', '.') ?></span>
								</div>
								<div class="col-sm-2">
									<input type="hidden" id="id_pembayaran_gaji" name="id_pembayaran_gaji" value="<?= $pembayaran_gaji['id'] ?>"  required>
								</div>
								<div class="col-sm-2">
									<input type="hidden" id="nrp" name="nrp" value="<?= $penerima_gaji['nrp'] ?>" required>
								</div>
							</div>
						</div>
						<div class="box-footer">
							<a class="btn btn-primary btn-sm" href="<?= base_url('pembayaran_gaji/daftar_penerima_gaji/' . $pembayaran_gaji['id']) ?>">
								<i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>
						</div>
					</form>
				</div>
			</div>
		</div>
		
	</section>
</div>