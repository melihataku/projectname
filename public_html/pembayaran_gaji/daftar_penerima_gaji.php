<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Penggajian
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?= base_url() ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
			<li><a href="<?= base_url('pembayaran_gaji') ?>">Pembayaran Gaji</a></li>
			<li class="active">Daftar Penerima Gaji</li>
		</ol>
	</section>
	<section class="content">
		<?php if($this->session->flashdata('msg_g')) : ?>
			<div class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h5><i class="icon fa fa-ban"></i> <?= $this->session->flashdata('msg_g') ?></h5>
			</div>
		<?php elseif($this->session->flashdata('msg')) : ?>
			<div class="alert alert-success alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h5><i class="icon fa fa-check"></i> <?= $this->session->flashdata('msg') ?></h5>
			</div>
		<?php endif; ?>
		<div class="row">
			<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Daftar Penerima Gaji: <?= $pembayaran_gaji['kode_bayar_gaji'] ?></h3>
				</div>
				<div class="box-body">
					<table id="tabel" class="table table-bordered table-hover table-striped">
						<thead>
							<tr>
								<th class="col-lg-1">No.</th>
								<th class="col-lg-1">NRP</th>
								<th class="col-lg-5">Nama Personil</th>
								<th class="col-lg-2">Pangkat</th>
								<th class="col-lg-2">Jabatan</th>
								<th class="col-lg-1">Tambahkan Detail Penghasilan</th>
							</tr>
						</thead>
						<tbody>
							<?php
								if ($penerima_gaji != FALSE) :
									foreach ($penerima_gaji as $key => $res) :
									?>
										<tr>
											<td><?= $key+1 ?></td>
											<td><?= $res['nrp'] ?></td>
											<td><?= $res['nama_depan'] . ' ' . $res['nama_belakang']?></td>
											<td><?= $res['nama_pangkat'] ?></td>
											<td><?= $res['nama_jabatan'] ?></td>
											<td>
												<?php if($sess['username'] == 'kasium') : ?>
													<?php if ($res['dibuat'] == 1) : ?>
														<a class="btn btn-info btn-sm" href="<?= base_url('pembayaran_gaji/edit_penghasilan/' . $pembayaran_gaji['id'] . '/' . $res['nrp']) ?>">
															<i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
														</a>
													<?php else : ?>
														<a class="btn btn-info btn-sm" href="<?= base_url('pembayaran_gaji/tambah_penghasilan/' . $pembayaran_gaji['id'] . '/' . $res['nrp']) ?>">
															<i class="fa fa-pencil-square-o" aria-hidden="true"></i> Tambahkan
														</a>
													<?php endif; ?>
												<?php else : ?>
													<a class="btn btn-info btn-sm" href="<?= base_url('pembayaran_gaji/detail_penghasilan/' . $pembayaran_gaji['id'] . '/' . $res['nrp']) ?>">
														<i class="fa fa-pencil-square-o" aria-hidden="true"></i> Lihat
													</a>
												<?php endif; ?>
											</td>
										</tr>
									<?php
									endforeach;
								endif;
							?>      
						</tbody>
					</table>
				</div>
				<div class="box-footer">
					<a class="btn btn-primary btn-sm" href="<?= base_url('pembayaran_gaji/') ?>">
						<i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>
					<?php if($pembayaran_gaji['disetujui'] == 1) : ?>
						<a class="btn btn-primary btn-sm disabled" href="<?= base_url('pembayaran_gaji/kirim_ke_kasikeu/' . $pembayaran_gaji['id']) ?>">
							<i class="fa fa-file-pdf-o" aria-hidden="true"></i> Sudah disetujui</a>
					<?php else: ?>
						<a class="btn btn-primary btn-sm" href="<?= base_url('pembayaran_gaji/kirim_ke_kasikeu/' . $pembayaran_gaji['id']) ?>">
							<i class="fa fa-file-pdf-o" aria-hidden="true"></i> Kirim ke Kasikeu</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
</div>