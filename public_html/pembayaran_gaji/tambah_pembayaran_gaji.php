
<?php 
$no_personil = '';
if($personil_aktif == 0) {
    $disabled = 'disabled';
    $no_personil = 'Tidak ada personil aktif.';
} else {
    $disabled = '';
} 
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Penggajian 
            <small>Tambah Pembayaran Gaji Baru</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
            <li><a href="<?= base_url('pembayaran_gaji') ?>"><i class="fa fa-dashboard"></i> Pembayaran Gaji</a></li>
            <li class="active">Pembayaran Gaji Baru</li>
        </ol>
    </section>

    <section class="content col-xs-8">
        <?php if($this->session->flashdata('msg_g')) : ?>
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fa fa-ban"></i> <?= $this->session->flashdata('msg_g') ?></h5>
            </div>
        <?php elseif($this->session->flashdata('msg')) : ?>
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fa fa-check"></i> <?= $this->session->flashdata('msg') ?></h5>
            </div>
        <?php elseif($no_personil) : ?>
            <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fa fa-warning"></i> <?= $no_personil ?></h5>
            </div>
        <?php endif; ?>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah Pembayaran Gaji Baru</h3>
            </div>
            <form role="form" class="form-horizontal" enctype="multipart/form-data" action="<?= base_url('pembayaran_gaji/do_post') ?>" method="POST">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-5">
                            <label>Kode Bayar</label>
                            <input type="text" class="form-control" id="kode_bayar_gaji" name="kode_bayar_gaji" <?= $disabled ?> autofocus>
                        </div>
                        <div class="col-xs-3">
                            <label>Jumlah Personil</label>
                            <input type="number" class="form-control" id="jumlah_personil" name="jumlah_personil" value="<?= $personil_aktif ?>" min="<?= $personil_aktif ?>" max="<?= $personil_aktif ?>" <?= $disabled ?>>
                        </div>
                        <div class="col-xs-8">
                            <label>Date range:</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="periode" name="periode" <?= $disabled ?> autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="col-xs-10">
                        <a class="btn btn-primary btn-sm" href="<?= base_url('pembayaran_gaji') ?>">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>
                    </div>
                    <div class="col-xs-2">
                        <button type="submit" name="do_post" class="btn btn-primary btn-sm">Tambahkan</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>