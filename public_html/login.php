<div class="login-box">
  <div class="login-logo">
    <img src="<?= base_url() ?>assets/img/Lambang_Polri-bw.png" height="29%" width="50%">
  </div>
  <?php if ($this->session->flashdata('msg_g')) : ?>
      <div class="alert alert-warning alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h4><i class="icon fa fa-warning"></i> <?= $this->session->flashdata('msg_g'); ?></h4>
      </div>
  <?php endif; ?>
  <?php if ($this->session->flashdata('msg')) : ?>
      <div class="alert alert-success alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h4><i class="icon fa fa-check"></i> <?= $this->session->flashdata('msg'); ?></h4>
      </div>
  <?php endif; ?>
  <div class="login-box-body">
    <p class="login-box-msg">Halaman Masuk</p>
    <form action="<?= base_url('login/do_login/') ?>" method="post">
      <div class="form-group has-feedback">
        <input name="nrp" type="text" class="form-control" placeholder="NRP">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input name="password" type="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-4">
          <button type="submit" name="do_login" class="btn btn-primary btn-block btn-flat">Masuk</button>
        </div>
        <div class="col-xs-4">
        </div>
      </div>
    </form>
  </div>
</div>