<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Penggajian
			<small>Beranda</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?= base_url() ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-aqua">
				<div class="inner">
					<h3><?= $total_personil ?></h3>
					<p>Total Personil</p>
				</div>
			</div>
			</div>
			<!-- ./col -->
			<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-green">
				<div class="inner">
					<h3><?= $personil_aktif ?></h3>
					<p>Personil Aktif</p>
				</div>
			</div>
			</div>
			<!-- ./col -->
			<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-yellow">
				<div class="inner">
					<h3><?= $personil_nonaktif ?></h3>
					<p>Personil Tidak Aktif</p>
				</div>
			</div>
			</div>
			<!-- ./col -->
			<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-red">
				<div class="inner">
					<h3>65</h3>
					<p>Unique Visitors</p>
				</div>
			</div>
			</div>
			<!-- ./col -->
		</div>
	</section>
</div>