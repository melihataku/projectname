<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Penggajian
			<small>Tambah Personil</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?= base_url() ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
			<li><a href="<?= base_url('pembayaran_gaji') ?>"><i class="fa fa-dashboard"></i> Personil</a></li>
			<li class="active">Tambah Personil</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-8">
				<?php if($this->session->flashdata('msg_g')) : ?>
					<div class="alert alert-danger alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<h5><i class="icon fa fa-ban"></i> <?= $this->session->flashdata('msg_g') ?></h5>
					</div>
				<?php elseif($this->session->flashdata('msg')) : ?>
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<h5><i class="icon fa fa-check"></i> <?= $this->session->flashdata('msg') ?></h5>
					</div>
				<?php endif; ?>
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Tambah Personil</h3>
					</div>
					<form role="form" class="form-horizontal" enctype="multipart/form-data" action="<?= base_url('personil/do_post') ?>" method="POST">
						<div class="box-body">
							<div class="row">
								<div class="col-xs-5">
									<label>Nama Depan</label>
									<input type="text" class="form-control box-border-radius" id="nama_depan" name="nama_depan" required autofocus>
								</div>
								<div class="col-xs-7">
									<label>Nama Belakang</label>
									<input type="text" class="form-control box-border-radius" id="nama_belakang" name="nama_belakang">
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4">
									<label>NRP</label>
									<input type="number" class="form-control box-border-radius" id="nrp" name="nrp" required>
								</div>
								<div class="col-xs-4">
									<label>NPWP</label>
									<input type="number" class="form-control box-border-radius" id="npwp" name="npwp">
								</div>
								<div class="col-xs-4">
									<label>Jenis Kelamin</label>
									<select name="jenis_kelamin" id="jenis_kelamin" class="form-control box-border-radius">
										<option disabled="disabled" selected="selected">Pilih Jenis Kelamin</option>
										<option value="1">Laki-laki</option>
										<option value="0">Perempuan</option>
									</select>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4">
									<label>Tanggal Lahir</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" class="form-control pull-right box-border-radius" id="tanggal_lahir" name="tanggal_lahir" required autocomplete="off">
									</div>
								</div>
								<div class="col-xs-4">
									<label>Status Perkawinan</label>
									<select name="status_kawin" id="status_kawin" class="form-control box-border-radius">
										<option value="2" disabled="disabled" selected="selected">Pilih Status Perkawinan</option>
										<option value="1">Kawin</option>
										<option value="0">Belum Kawin</option>
									</select>
								</div>
								<div class="col-xs-4">
									<label>Jumlah Anak</label>
									<input type="number" class="form-control box-border-radius" id="jumlah_anak" name="jumlah_anak" min="0">
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4">
									<label>Pangkat</label>
									<select name="id_pangkat" id="id_pangkat" class="form-control select">
										<option disabled="disabled" selected="selected">Pilih Pangkat</option>
										<?php foreach($pangkat as $res) : ?>
											<option value="<?= $res['id'] ?>"><?= $res['nama_pangkat'] ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="col-xs-4">
									<label>Jabatan</label>
									<select name="id_jabatan" id="id_jabatan" class="form-control select">
										<option disabled="disabled" selected="selected">Pilih Jabatan</option>
										<?php foreach($jabatan as $res) : ?>
											<option value="<?= $res['id'] ?>"><?= $res['nama_jabatan'] ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="col-xs-4">
									<label>Masa Kerja</label>
									<input type="number" class="form-control box-border-radius" id="masa_kerja" name="masa_kerja" min="0" required>
								</div>
							</div>
						</div>
						<div class="box-footer">
							<div class="col-xs-10">
								<a class="btn btn-primary btn-sm" href="<?= base_url('personil') ?>">
									<i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>
							</div>
							<div class="col-xs-2">
								<button type="submit" name="do_post" class="btn btn-primary btn-sm">Tambahkan</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>
<script>
$(document).ready(function () {
	$('#tanggal_lahir').datepicker({
		autoclose: true
	});
	$('.select').select2();
});
</script>