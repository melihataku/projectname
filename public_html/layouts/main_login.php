<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<html>
    <head>
        <meta charset = "utf-8">
        <meta http-equiv = "X-UA-Compatible" content = "IE=edge">
        <title><?= $title ?></title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="<?= base_url() ?>/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>/assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>/assets/css/ionicons.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>/assets/css/AdminLTE.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>/assets/css/skins/_all-skins.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>/assets/css/datepicker3.css">
        <link rel="stylesheet" href="<?= base_url() ?>/assets/select2/select2.min.css">
    </head>
    <body class="hold-transition login-page">
        <?= $content ?>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/app.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>assets/select2/select2.full.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#tanggal_lahir').datepicker({
                    autoclose: true
                });
                $('.select').select2();
            });
        </script>
    </body>
</html>
