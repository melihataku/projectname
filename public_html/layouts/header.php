<header class="main-header">
    <a href="<?= base_url() ?>" class="logo">
      <span class="logo-mini"><b>G</b>aji</span>
      <span class="logo-lg"><b>Penggajian</b></span>
    </a>
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?= base_url() ?>assets/img/user.png" class="user-image" alt="<?= $sess['nama_depan'] . ' ' . $sess['nama_belakang'] ?> Image">
              <span class="hidden-xs"><?= $sess['nama_depan'] . ' ' . $sess['nama_belakang'] ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-header">
                <img src="<?= base_url() ?>assets/img/user.png" class="img-circle" alt="<?= $sess['nama_depan'] . ' ' . $sess['nama_belakang'] ?> Image">
                <p>
                  <?= $sess['nama_depan'] . ' ' . $sess['nama_belakang'] ?>
                  <small><?= $sess['nama_pangkat'] . '-' . $sess['nama_jabatan'] ?></small>
                </p>
              </li>
              <li class="user-body">
                <div class="pull-right">
                  <a href="<?= base_url('logout') ?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>