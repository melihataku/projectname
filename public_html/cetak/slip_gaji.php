<?php

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Custom Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
// require_once('tcpdf_include.php');


// Extend the TCPDF class to create custom Header and Footer
setlocale(LC_ALL, array('IND', 'id_ID'));
class MYPDF extends TCPDF {

	//Page header
	public function Header() {
		// Logo
		$image_file = ASSETS.'logo_kepolisian.png';
		$this->Image($image_file, 20, 10, 20, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		// Set font
		// $this->SetFont('helvetica', '', 10);
		// Title
		// $this->Cell(0, 0, '', 0, false, 'L', 0, '', 0, false, 'M', 'M');
	}

	// Page footer
	public function Footer() {
		// Position at 15 mm from bottom
		// $this->SetY(-15);
		// Set font
		// $this->SetFont('helvetica', 'I', 8);
		// Page number
		// $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('ANDRIZA YUNI & YENNY SANTI MARIA');
$pdf->SetTitle('SLIP GAJI');
$pdf->SetSubject('SLIP GAJI');
$pdf->SetKeywords('SLIP GAJI, GAJI');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', 'B', 12);
foreach ($slip_gaji as $res) {
	// bikin halaman
	$pdf->AddPage();

	$html = '
	<div style="text-align:center;"><b><u>SURAT KETERANGAN PERINCIAN GAJI</u></b></div>
	<div style="text-align:left;font-weight:normal;">
		<span>Yang bertanda tangan di bawah ini saya:</span>
		<br><br>
		<table style="text-align:left;font-weight:normal;">
			<tr>
				<td>Nama</td>
				<td>: ' . $sess['nama_depan'] . ' ' . $sess['nama_belakang'] . '</td>
				<td></td>
			</tr>
			<tr>
				<td>Pangkat/NRP</td>
				<td>: ' . $sess['nama_pangkat'] . '/' . $sess['nrp'] . '</td>
			</tr>
			<tr>
				<td>Jabatan</td>
				<td>: ' . $sess['nama_jabatan'] . '</td>
			</tr>
			<tr>
				<td>Kesatuan</td>
				<td>: POLSEK KALIDERES</td>
			</tr>
		</table>
		<br><br>
		<span>Menerangkan dengan sebenarnya bahwa nama: ' . $res['nama_depan'] . ' ' . $res['nama_belakang'] . ', Pangkat ' . $res['nama_pangkat'] . ', NRP ' . $res['nrp'] . ', Jabatan ' . $res['nama_jabatan'] . ' Polsek Kalideres Polres Metro Jakarta Barat mempunyai penghasilan gaji bulan ' . date("F Y", $res['periode_gaji_dari']) . ':</span>
		<br><br>
		<table style="text-align:left;font-weight:normal;">
			<tr>
				<td style="width:35%">1. Gaji Pokok</td>
				<td style="width:19%">: Rp. ' . number_format($res['gaji_pokok'], 0,',', '.') . ',-</td>
				<td style="width:46%"></td>
			</tr>
			<tr>
				<td>2. Tunjangan Istri/Suami</td>
				<td>: Rp. '. number_format($res['tunjangan_suami_istri'], 0,',', '.') .',-</td>
			</tr>
			<tr>
				<td>3. Tunjangan Anak</td>
				<td>: Rp. '. number_format($res['tunjangan_anak'], 0,',', '.') .',-</td>
			</tr>
			<tr>
				<td>4. Uang Lauk Pauk</td>
				<td>: Rp. '. number_format($res['tunjangan_lauk_pauk'], 0,',', '.') .',-</td>
			</tr>
			<tr>
				<td>5. Tunjangan Umum</td>
				<td>: Rp. '. number_format($res['tunjangan_umum'], 0,',', '.') .',-</td>
			</tr>
			<tr>
				<td>6. Tunjangan Beras</td>
				<td>: Rp. '. number_format($res['tunjangan_beras'], 0,',', '.') .',-</td>
			</tr>
			<tr>
				<td>7. Tunjangan Bhabinkamtibmas</td>
				<td>: Rp. '. number_format($res['tunjangan_profesi'], 0,',', '.') .',-</td>
			</tr>
			<tr>
				<td>8. ...............................................</td>
				<td>: Rp. </td>
			</tr>
			<tr style="font-weight:bold;">
				<td style="text-align:center;">Jumlah</td>
				<td>:_____________</td>
				<td>Rp. '. number_format($res['jumlah_penghasilan_kotor'], 0,',', '.') .',-</td>
			</tr>
		</table>
		<br><br>
		<span>Dengan potongan-potongan sebagai berikut:</span>
		<br><br>
		<table style="text-align:left;font-weight:normal;">
			<tr>
				<td style="width:35%">1. IWP</td>
				<td style="width:19%">: Rp. '. number_format($res['iwp'], 0,',', '.') .',-</td>
				<td style="width:46%"></td>
			</tr>
			<tr>
				<td>2. BPJS</td>
				<td>: Rp. '. number_format($res['bpjs'], 0,',', '.') .',-</td>
			</tr>
			<tr>
				<td>3. PPh Pasal 21</td>
				<td>: Rp. '. number_format($res['pph_ps_21'], 0,',', '.') .',-</td>
			</tr>
			<tr>
				<td>4. Utang</td>
				<td>: Rp. '. number_format($res['utang'], 0,',', '.') .',-</td>
			</tr>
			<tr style="font-weight:bold;">
				<td>Jumlah potongan</td>
				<td></td>
				<td><u>Rp. '. number_format($res['jumlah_potongan'], 0,',', '.') .',-</u></td>
			</tr>
			<tr style="font-weight:bold;">
				<td>Jumlah yang diterima</td>
				<td></td>
				<td><u>Rp. '. number_format($res['jumlah_penghasilan_bersih'], 0,',', '.') .',-</u></td>
			</tr>
		</table>
		<br><br>
		<div style="text-align:right;">
			<span>Jakarta, '. date("d F Y", time()) .'&nbsp;&nbsp;&nbsp;&nbsp;</span>
			<br>
			<span>
				' . $sess['nama_jabatan'] . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</span>
			<br><br><br><br><br>
			<span>
				<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $sess['nama_depan'] . ' ' . $sess['nama_belakang'] . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>
			</span>
			<br>
			<span>' . $sess['nama_pangkat'] . ' NRP ' . $sess['nrp'] . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
		</div>
	</div>
	';

	$pdf->writeHTML($html, true, false, true, false, 'C');
}

// ---------------------------------------------------------
//Close and output PDF document
ob_clean();
$pdf->Output('SLIP_GAJI_'. date("d_F_Y", $res['periode_gaji_dari']) .'.pdf', 'I');