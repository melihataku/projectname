<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_pembayaran_gaji extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_all($table) {//ngambil semua data pembayaran gaji, yang sudah ditanda tangan/belum
        return $this->db->select()->from($table)->order_by('id', 'DESC')->get()->result_array();
    }

    function get($id) {//ngambil kode pembayaran gaji
        return $this->db->get_where('pembayaran_gaji', array('id' => $id))->row_array();
    }

    function get_slip_gaji($kode_bayar_gaji) {//ngambil kode pembayaran gaji
        return $this->db->select('id')->from('pembayaran_gaji')->where('kode_bayar_gaji', $kode_bayar_gaji)->get()->row_array();
    }

    function get_latest($table) {//cari pembayaran gaji yang paling terakhir (login pake akun kaurkeu)
        return $this->db->select('disetujui')->from($table)->limit(1)->order_by('id', 'DESC')->get()->row_array();
    }

    function post($param) {//simpan pembuatan pembayaran gaji (sebelum nambah data perincian penghasilan)
        $this->db->trans_start();
        $this->db->insert('pembayaran_gaji', $param);
        $id = $this->db->insert_id();
        $this->db->trans_complete();
        if ($this->db->trans_status() == FALSE) {
            return FALSE;
        } else {
            return $id;
        }
    }

    function update($id, $param) {
        // print_r($id);print_r($param);exit;
        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->update('pembayaran_gaji', $param);
        $this->db->trans_complete();
        if ($this->db->trans_status() == FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function update_jumlah_gaji($id_pembayaran_gaji) {
        $query = $this->db->select_sum('jumlah_penghasilan_bersih')->from('penerima_gaji')->where('id_pembayaran_gaji', $id_pembayaran_gaji)->get()->row_array();
        $this->db->trans_start();
        $this->db->set('total_gaji', (int) $query['jumlah_penghasilan_bersih'], FALSE);
        $this->db->where('id', $id_pembayaran_gaji);
        $this->db->update('pembayaran_gaji');
        $this->db->trans_complete();
        if ($this->db->trans_status() == FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
}
