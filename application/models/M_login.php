<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_login extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function login($nrp, $password) {
        if ($nrpIsExist = $this->db->get_where('cms_profile', array('nrp' => $nrp))->row_array()) {
            $userIsExist = $this->db->get_where('user', array('profile_id' => $nrpIsExist['id']))->row_array();
            if (password_verify($password, $userIsExist['password_hash'])) {
                $this->db->select('a.id, a.username, b.nrp, b.nama_depan, b.nama_belakang, b.jenis_kelamin, b.created_at, c.nama_pangkat, d.nama_jabatan');
                $this->db->from('user a');
                $this->db->join('cms_profile b', 'b.id=a.profile_id');
                $this->db->join('ms_pangkat c', 'c.id=b.id_pangkat');
                $this->db->join('ms_jabatan d', 'd.id=b.id_jabatan');
                $this->db->where('b.nrp', $nrp);
                $result = $this->db->get();
                return $result->row_array();
            }
            return FALSE;
        }
        return FALSE;
    }
}
