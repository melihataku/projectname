<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_personil extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_all($table) {
        if ($table == 'ms_jabatan') {
            $names = array('Kasium', 'Kasikeu');
            return $this->db->select()->from($table)->or_where_not_in('nama_jabatan', $names)->get()->result_array();
        } elseif ($table == 'ms_pangkat') {
            $names = array('IPTU');
            return $this->db->select()->from($table)->or_where_not_in('nama_pangkat', $names)->get()->result_array();
        }
    }

    function get_register($table) {
        if ($table == 'ms_jabatan') {
            $names = array('Bhabinkamtibmas');
            return $this->db->select()->from($table)->or_where_not_in('nama_jabatan', $names)->get()->result_array();
        } elseif ($table == 'ms_pangkat') {
            $names = array('AIPTU');
            return $this->db->select()->from($table)->or_where_not_in('nama_pangkat', $names)->get()->result_array();
        }
    }

    function post($param) {//simpan perincian penghasilan
        $this->db->trans_start();
        $this->db->insert('cms_profile', $param);
        $id = $this->db->insert_id();
        $this->db->trans_complete();
        if ($this->db->trans_status() == FALSE) {
            return FALSE;
        } else {
            return $id;
        }
    }

    function post_user($param) {//simpan perincian penghasilan
        $this->db->trans_start();
        $this->db->insert('user', $param);
        $this->db->trans_complete();
        if ($this->db->trans_status() == FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function update($nrp, $param) {//simpan perincian penghasilan
        $this->db->trans_start();
        $this->db->where('nrp', $nrp);
        $this->db->update('cms_profile', $param);
        $this->db->trans_complete();
        if ($this->db->trans_status() == FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function check($table, $param, $var) {
        return $this->db->get_where($table, array($var => $param))->num_rows();
    }

    function check_username($param, $var) {
        return $this->db->get_where('user', array($var => $param))->num_rows();
    }
}
