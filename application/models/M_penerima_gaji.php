<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_penerima_gaji extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_all_personil_with_status($status) {//mendapatkan semua data personil aktif
        $this->db->select('a.id AS profile_id, a.id_pangkat, a.id_jabatan, a.status, a.nrp, a.nama_depan, 
                        a.nama_belakang, a.jenis_kelamin, a.created_at, c.nama_pangkat, d.nama_jabatan,
                        a.npwp, a.jumlah_anak, a.status_kawin, a.masa_kerja, a.tanggal_lahir');
		$this->db->from('cms_profile a');
		$this->db->join('ms_pangkat c', 'c.id=a.id_pangkat');
        $this->db->join('ms_jabatan d', 'd.id=a.id_jabatan');
        if ($status == 'aktif') {
            $var = 1;
        } elseif ($status == 'nonaktif') {
            $var = 2;
            $id_jabatan = array(1, 2);//apus ini kalo perlu
            $this->db->where_not_in('a.id_jabatan', $id_jabatan);
        }
		$this->db->where('a.status', $var);
		$this->db->order_by('a.created_at', 'DESC');
		$query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return FALSE;
    }

    function get_all_personil() {//mendapatkan semua data personil aktif
        $this->db->select('a.id AS profile_id, a.id_pangkat, a.id_jabatan, a.status, a.nrp, a.nama_depan, 
                        a.nama_belakang, a.jenis_kelamin, a.created_at, c.nama_pangkat, d.nama_jabatan,
                        a.npwp, a.jumlah_anak, a.status_kawin, a.masa_kerja, a.tanggal_lahir');
		$this->db->from('cms_profile a');
		$this->db->join('ms_pangkat c', 'c.id=a.id_pangkat');
        $this->db->join('ms_jabatan d', 'd.id=a.id_jabatan');
        $id_jabatan = array(1, 2);//apus ini kalo perlu
		$this->db->where_not_in('a.id_jabatan', $id_jabatan);
		$this->db->order_by('a.nrp', 'DESC');
		$query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return FALSE;
    }

    function get_last_personil() {//cari pembayaran gaji yang paling terakhir (login pake akun kaurkeu)
        $id_jabatan = array(1, 2);//apus ini kalo perlu
        return $this->db->select()->from('cms_profile')->limit(1)->where_not_in('id_jabatan', $id_jabatan)->order_by('created_at', 'DESC')->get()->row_array();
    }

    function get_all($id_pembayaran_gaji) {//mendapatkan semua data personil aktif untuk di tampilkan di penggajian/pembayaran_gaji/tambahkan_data/
        $this->db->select('a.*, b.nama_pangkat, c.nama_jabatan, d.status, d.nrp, d.nama_depan, d.nama_belakang, 
                        d.jenis_kelamin, d.npwp, d.jumlah_anak, d.status_kawin, d.masa_kerja, d.tanggal_lahir');
        $this->db->from('penerima_gaji a');
		$this->db->join('cms_profile d', 'd.id=a.profile_id');
		$this->db->join('ms_pangkat b', 'b.id=d.id_pangkat');
		$this->db->join('ms_jabatan c', 'c.id=d.id_jabatan');
		$this->db->where('a.id_pembayaran_gaji', $id_pembayaran_gaji);
		$this->db->order_by('d.nrp', 'DESC');
		$query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return FALSE;
    }

    function get($nrp) {//mendapatkan data personil sesuai nrp
        $this->db->select('a.id, a.id_pangkat, a.id_jabatan, a.status, a.nrp, a.nama_depan, a.nama_belakang, a.npwp, 
                        a.jumlah_anak, a.status_kawin, a.masa_kerja, a.tanggal_lahir, a.id_pangkat, a.id_jabatan, 
                        a.status, a.jenis_kelamin, b.nama_pangkat, c.nama_jabatan');
        $this->db->from('cms_profile a');
		$this->db->join('ms_pangkat b', 'b.id=a.id_pangkat');
		$this->db->join('ms_jabatan c', 'c.id=a.id_jabatan');
		$this->db->where('a.nrp', $nrp);
		$query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return FALSE;
    }

    function get_penghasilan($nrp, $id_pembayaran_gaji) {//mendapatkan data gaji sesuai nrp dan id pembayaran gaji
        $this->db->select('a.id, a.id_pangkat, a.id_jabatan, a.status, a.nrp, a.nama_depan, a.nama_belakang, 
                        a.jenis_kelamin, c.nama_pangkat, d.nama_jabatan, e.gaji_pokok, e.tunjangan_suami_istri,
                        e.tunjangan_anak, e.tunjangan_umum, e.gaji_bruto, e.tunjangan_lauk_pauk, e.tunjangan_beras, 
                        e.tunjangan_profesi, e.jumlah_penghasilan_kotor, e.iwp, e.bpjs, e.pph_ps_21, e.utang, 
                        e.jumlah_potongan, e.jumlah_penghasilan_bersih');
        $this->db->from('cms_profile a');
		$this->db->join('ms_pangkat c', 'c.id=a.id_pangkat');
		$this->db->join('ms_jabatan d', 'd.id=a.id_jabatan');
		$this->db->join('penerima_gaji e', 'e.profile_id=a.id');
		$this->db->where('a.nrp', $nrp);
		$this->db->where('e.id_pembayaran_gaji', $id_pembayaran_gaji);
		$query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return FALSE;
    }

    function post($profile_id, $id_pembayaran_gaji, $param) {//simpan perincian penghasilan
        $this->db->trans_start();
		$this->db->where('profile_id', $profile_id);
		$this->db->where('id_pembayaran_gaji', $id_pembayaran_gaji);
        $this->db->update('penerima_gaji', $param);
        $this->db->trans_complete();
        if ($this->db->trans_status() == FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function post_batch($param) {//simpan perincian penghasilan
        $this->db->trans_start();
        $this->db->insert_batch('penerima_gaji', $param);
        $this->db->trans_complete();
        if ($this->db->trans_status() == FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function get_slip_gaji($kode_bayar_gaji) {//mendapatkan data personil sesuai nrp
        $this->db->select('b.periode_gaji_dari, b.periode_gaji_sampai, c.nama_depan, c.nama_belakang, 
                        d.nama_pangkat, e.nama_jabatan, c.nrp, a.*');
        $this->db->from('penerima_gaji a');
        $this->db->join('pembayaran_gaji b', 'b.id=a.id_pembayaran_gaji');
        $this->db->join('cms_profile c', 'c.id=a.profile_id');
		$this->db->join('ms_pangkat d', 'd.id=c.id_pangkat');
		$this->db->join('ms_jabatan e', 'e.id=c.id_jabatan');
		$this->db->where('b.kode_bayar_gaji', $kode_bayar_gaji);
		$query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return FALSE;
    }
    
    function get_laporan($kode_bayar_gaji) {//mendapatkan data personil sesuai nrp
        $this->db->select('a.periode_gaji_dari, a.periode_gaji_sampai, c.npwp, c.jenis_kelamin, c.masa_kerja,
                        c.jumlah_anak, c.status_kawin, c.tanggal_lahir, c.nama_depan, c.nama_belakang, d.nama_pangkat,
                        e.nama_jabatan, c.nrp, b.*');
        $this->db->from('pembayaran_gaji a');
        $this->db->join('penerima_gaji b', 'a.id=b.id_pembayaran_gaji');
        $this->db->join('cms_profile c', 'c.id=b.profile_id');
		$this->db->join('ms_pangkat d', 'd.id=c.id_pangkat');
		$this->db->join('ms_jabatan e', 'e.id=c.id_jabatan');
		$this->db->where('a.kode_bayar_gaji', $kode_bayar_gaji);
		$query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return FALSE;
    }

    function get_periode($kode_bayar_gaji) {//ngambil kode pembayaran gaji
        return $this->db->get_where('pembayaran_gaji', array('kode_bayar_gaji' => $kode_bayar_gaji))->row_array();
    }
}
