<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_pembayaran_gaji extends CI_Migration {

    public function up() {
        echo "Start Master Jabatan Migration \n";

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 4,
                'null' => FALSE,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'kode_bayar_gaji' => array(
                'type' => 'VARCHAR',
                'constraint' => 20,
                'null' => FALSE
            ),
            'periode_gaji_dari' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE
            ),
            'periode_gaji_sampai' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE
            ),
            'jumlah_personil' => array(
                'type' => 'INT',
                'constraint' => 5,
                'null' => TRUE
            ),
            'total_gaji' => array(
                'type' => 'BIGINT',
                'constraint' => 20,
                'null' => TRUE
            ),
            'disetujui' => array(
                'type' => 'INT',
                'constraint' => 1,
                'null' => FALSE
            ),
            'slip_gaji_dibuat' => array(
                'type' => 'INT',
                'constraint' => 1,
                'null' => FALSE
            ),
            'created_by' => array(
                'type' => 'INT',
                'constraint' => 4,
                'null' => TRUE
            ),
            'created_at' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE
            ),
            'updated_by' => array(
                'type' => 'INT',
                'constraint' => 4,
                'null' => TRUE
            ),
            'updated_at' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        if ($this->dbforge->create_table('pembayaran_gaji')) {
            echo "Status : Success \n";
        } else {
            echo "Status : FAILED \n";
        }
    }

    public function down() {
        $this->dbforge->drop_table('pembayaran_gaji');
    }

}
