<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_user extends CI_Migration {

    public function up() {
        echo "Start User Migration \n";

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => FALSE,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
			'profile_id' => array(
				'type' => 'INT',
				'constraint' => 4,
				'unsigned' => TRUE
			),
            'username' => array(
                'type' => 'VARCHAR',
                'constraint' => 10
            ),
            // 'auth_key' => array(
            //     'type' => 'VARCHAR',
            //     'constraint' => 32
            // ),
            'password_hash' => array(
                'type' => 'VARCHAR',
                'constraint' => 255
            ),
            // 'password_reset_token' => array(
            //     'type' => 'VARCHAR',
            //     'constraint' => 255
            // ),
            // 'email' => array(
            //     'type' => 'VARCHAR',
            //     'constraint' => 100
            // ),
            'created_at' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE
            ),
            'updated_at' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        if ($this->dbforge->create_table('user')) {
            echo "Creating foreign key for id_pangkat \n";
            $this->db->query('ALTER TABLE `cms_profile` ADD KEY `idx-cms_profile-id_pangkat` (`id_pangkat`)');
            $this->db->query('ALTER TABLE `cms_profile` ADD CONSTRAINT `fk-cms_profile-id_pangkat` FOREIGN KEY (`id_pangkat`) REFERENCES `ms_pangkat` (`id`)');
            echo "Creating foreign key for id_jabatan \n";
            $this->db->query('ALTER TABLE `cms_profile` ADD KEY `idx-cms_profile-id_jabatan` (`id_jabatan`)');
            $this->db->query('ALTER TABLE `cms_profile` ADD CONSTRAINT `fk-cms_profile-id_jabatan` FOREIGN KEY (`id_jabatan`) REFERENCES `ms_jabatan` (`id`)');
            
            echo "Creating foreign key for profile_id \n";
			$this->db->query('ALTER TABLE `penerima_gaji` ADD KEY `idx-penerima_gaji-profile_id` (`profile_id`)');
            $this->db->query('ALTER TABLE `penerima_gaji` ADD CONSTRAINT `fk-penerima_gaji-profile_id` FOREIGN KEY (`profile_id`) REFERENCES `cms_profile` (`id`)');
            echo "Creating foreign key for id_pembayaran_gaji \n";
            $this->db->query('ALTER TABLE `penerima_gaji` ADD KEY `idx-penerima_gaji-id_pembayaran_gaji` (`id_pembayaran_gaji`)');
            $this->db->query('ALTER TABLE `penerima_gaji` ADD CONSTRAINT `fk-penerima_gaji-id_pembayaran_gaji` FOREIGN KEY (`id_pembayaran_gaji`) REFERENCES `pembayaran_gaji` (`id`)');
            
            echo "Creating foreign key for profile_id \n";
			$this->db->query('ALTER TABLE `user` ADD KEY `idx-user-profile_id` (`profile_id`)');
            $this->db->query('ALTER TABLE `user` ADD CONSTRAINT `fk-user-profile_id` FOREIGN KEY (`profile_id`) REFERENCES `cms_profile` (`id`)');
            echo "Status : Success \n";
            
            echo "Creating help for 'disetujui', 'slip_gaji_dibuat' on table 'pembayaran_gaji' \n";
            $this->db->query('ALTER TABLE `pembayaran_gaji` MODIFY COLUMN `slip_gaji_dibuat` INT(1) NOT NULL COMMENT "membuat slip gaji setelah disetujui kasikeu, 1 berarti sudah, 2 berarti belum"');
            $this->db->query('ALTER TABLE `pembayaran_gaji` MODIFY COLUMN `disetujui` INT(1) NOT NULL COMMENT "disetujui oleh kasikeu, 1 berarti sudah, 2 berarti belum"');
            
            echo "Creating help for 'gaji_bruto', 'jumlah_penghasilan_kotor', 'jumlah_penghasilan_bersih', 'dibuat' on table 'penerima_gaji' \n";
            $this->db->query('ALTER TABLE `penerima_gaji` MODIFY COLUMN `gaji_bruto` INT(9) NOT NULL COMMENT "gaji_pokok+tunjangan_suami_istri+tunjangan_anak"');
            $this->db->query('ALTER TABLE `penerima_gaji` MODIFY COLUMN `jumlah_penghasilan_kotor` INT(9) NOT NULL COMMENT "gaji_bruto+semua_tunjangan"');
            $this->db->query('ALTER TABLE `penerima_gaji` MODIFY COLUMN `jumlah_penghasilan_bersih` INT(9) NOT NULL COMMENT "jumlah_penghasilan_kotor-jumlah_potongan"');
            $this->db->query('ALTER TABLE `penerima_gaji` MODIFY COLUMN `dibuat` INT(1) NULL COMMENT "membuat daftar penerima gaji oleh kasium sebelum ditandatangan, 1 berarti sudah ditambahkan datanya"');
            
            echo "Creating help for 'status', 'jenis_kelamin', 'status_kawin' on table 'cms_profile' \n";
            $this->db->query('ALTER TABLE `cms_profile` MODIFY COLUMN `status` INT(1) NOT NULL COMMENT "1 berarti aktif, 2 berarti tidak aktif"');
            $this->db->query('ALTER TABLE `cms_profile` MODIFY COLUMN `status_kawin` INT(1) NOT NULL COMMENT "1 berarti kawin, 0 berarti belum kawin"');
            $this->db->query('ALTER TABLE `cms_profile` MODIFY COLUMN `jenis_kelamin` INT(1) NOT NULL COMMENT "1 berarti laki-laki, 0 berarti perempuan"');

            echo "ALL DONE";
        } else {
            echo "Status : FAILED \n";
        }
    }

    public function down() {
        $this->db->query('ALTER TABLE `user` DROP FOREIGN KEY `fk-user-access_type`');
        $this->db->query('DROP INDEX `user.idx-user-access_type`');
				
		$this->db->query('ALTER TABLE `user` DROP FOREIGN KEY `fk-user-profile_id`');
        $this->db->query('DROP INDEX `user.idx-user-profile_id`');
		
        $this->dbforge->drop_table('user');
    }
}
