<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_cms_profile extends CI_Migration {

    public function up() {
        echo "Start CMS Profile Migration \n";

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 4,
                'null' => FALSE,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'nrp' => array(
                'type' => 'INT',
                'constraint' => 8,
                'null' => FALSE
            ),
            'npwp' => array(
                'type' => 'BIGINT',
                'constraint' => 15,
                'null' => FALSE
            ),
            'tanggal_lahir' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => FALSE
            ),
            'nama_depan' => array(
                'type' => 'VARCHAR',
                'constraint' => 15,
                'null' => FALSE
            ),
            'nama_belakang' => array(
                'type' => 'VARCHAR',
                'constraint' => 45,
                'null' => TRUE
            ),
            'id_pangkat' => array(
                'type' => 'INT',
                'constraint' => 4,
                'unsigned' => TRUE
            ),
            'id_jabatan' => array(
                'type' => 'INT',
                'constraint' => 4,
                'unsigned' => TRUE
            ),
            'jenis_kelamin' => array(
                'type' => 'INT',
                'constraint' => 1,
                'null' => FALSE
            ),
            'status_kawin' => array(
                'type' => 'INT',
                'constraint' => 1,
                'null' => FALSE
            ),
            'jumlah_anak' => array(
                'type' => 'INT',
                'constraint' => 1
            ),
            'masa_kerja' => array(
                'type' => 'INT',
                'constraint' => 3
            ),
            'status' => array(
                'type' => 'INT',
                'constraint' => 1,
                'null' => FALSE
            ),
            'created_at' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE
            ),
            'created_by' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE
            ),
            'updated_at' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE
            ),
            'updated_by' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        if ($this->dbforge->create_table('cms_profile')) {
            echo "Status : Success \n";
        } else {
            echo "Status : FAILED \n";
        }
    }

    public function down() {
        $this->dbforge->drop_table('cms_profile');
    }

}
