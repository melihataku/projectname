<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_penerima_gaji extends CI_Migration {

    public function up() {
        echo "Start Penerima Gaji Migration \n";

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => FALSE,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'profile_id' => array(
                'type' => 'INT',
                'constraint' => 4,
                'null' => FALSE,
                'unsigned' => TRUE
            ),
            'id_pembayaran_gaji' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => FALSE,
                'default' => 0,
                'unsigned' => TRUE
            ),
            'gaji_pokok' => array(
                'type' => 'INT',
                'constraint' => 9,
                'null' => FALSE,
                'default' => 0,
            ),
            'tunjangan_suami_istri' => array(
                'type' => 'INT',
                'constraint' => 9,
                'null' => FALSE,
                'default' => 0,
            ),
            'tunjangan_anak' => array(
                'type' => 'INT',
                'constraint' => 9,
                'null' => FALSE,
                'default' => 0,
            ),
            'gaji_bruto' => array(
                'type' => 'INT',
                'constraint' => 9,
                'null' => FALSE,
                'default' => 0,
            ),
            'tunjangan_lauk_pauk' => array(
                'type' => 'INT',
                'constraint' => 7,
                'null' => FALSE,
                'default' => 0,
            ),
            'tunjangan_umum' => array(
                'type' => 'INT',
                'constraint' => 5,
                'null' => FALSE,
                'default' => 0,
            ),
            'tunjangan_beras' => array(
                'type' => 'INT',
                'constraint' => 9,
                'null' => FALSE,
                'default' => 0,
            ),
            'tunjangan_profesi' => array(
                'type' => 'INT',
                'constraint' => 9,
                'null' => FALSE,
                'default' => 0,
            ),
            'jumlah_penghasilan_kotor' => array(
                'type' => 'INT',
                'constraint' => 9,
                'null' => FALSE,
                'default' => 0,
            ),
            'iwp' => array(
                'type' => 'INT',
                'constraint' => 9,
                'null' => FALSE,
                'default' => 0,
            ),
            'bpjs' => array(
                'type' => 'INT',
                'constraint' => 9,
                'null' => FALSE,
                'default' => 0,
            ),
            'pph_ps_21' => array(
                'type' => 'INT',
                'constraint' => 9,
                'null' => FALSE,
                'default' => 0,
            ),
            'utang' => array(
                'type' => 'INT',
                'constraint' => 9,
                'null' => FALSE,
                'default' => 0,
            ),
            'jumlah_potongan' => array(
                'type' => 'INT',
                'constraint' => 9,
                'null' => FALSE,
                'default' => 0,
            ),
            'jumlah_penghasilan_bersih' => array(
                'type' => 'INT',
                'constraint' => 9,
                'null' => FALSE,
                'default' => 0,
            ),
            'dibuat' => array(
                'type' => 'INT',
                'constraint' => 1,
                'null' => FALSE,
                'default' => 0,
            ),
            'created_by' => array(
                'type' => 'INT',
                'constraint' => 4,
                'null' => TRUE
            ),
            'created_at' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE
            ),
            'updated_by' => array(
                'type' => 'INT',
                'constraint' => 4,
                'null' => TRUE
            ),
            'updated_at' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        if ($this->dbforge->create_table('penerima_gaji')) {
            echo "Status : Success \n";
        } else {
            echo "Status : FAILED \n";
        }
    }

    public function down() {
        $this->dbforge->drop_table('penerima_gaji');
    }

}
