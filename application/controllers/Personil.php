<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personil extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model(array('m_penerima_gaji', 'm_personil'));
		if ($sess = $this->session->userdata('user')) {
			//kalo udah login bisa masuk sini
			return TRUE;
		} else {
			//kalo gak login disuruh login dulu
			redirect('login'); 
		}
	}

	function sess() {
		//ambil session
		return $this->session->userdata('user');
	}

	public function index() {
		$data['sess'] = $this->session->userdata('user');
		$data['all_personil'] = $this->m_penerima_gaji->get_all_personil();
		$data['personil'] = $this->m_penerima_gaji->get_last_personil();
		$this->render->title = 'Penggajian | Personil';
		$this->render->data('personil/index', $data);
	}

	public function tambah_personil() {
		if (isset($_POST['do_post'])) {
			$this->do_post();
		} else {
			$data['sess'] = $this->session->userdata('user');
			$data['pangkat'] = $this->m_personil->get_all('ms_pangkat');//ngambil semua nama pangkat
			$data['jabatan'] = $this->m_personil->get_all('ms_jabatan');//ngambil semua nama jabatan
			$data['personil'] = $this->m_penerima_gaji->get_last_personil();
			$this->render->title = 'Penggajian | Tambah Personil';
			$this->render->data('personil/tambah_personil', $data);
		}
	}

	public function edit_personil($nrp) {
		if (isset($_POST['do_edit'])) {
			$this->do_edit();
		} else {
			$data['sess'] = $this->session->userdata('user');
			$data['data_personil'] = $this->m_penerima_gaji->get($nrp);
			$data['pangkat'] = $this->m_personil->get_all('ms_pangkat');
			$data['jabatan'] = $this->m_personil->get_all('ms_jabatan');
			$data['personil'] = $this->m_penerima_gaji->get_last_personil();
			$this->render->title = 'Penggajian | Edit Personil';
			$this->render->data('personil/edit_personil', $data);
		}
	}

	function do_post() {
		$data['sess'] = $this->sess();
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nama_depan', 'Nama Depan', 'trim|required|min_length[1]', array('required' => 'Nama Depan harus diisi.'));
		$this->form_validation->set_rules('nama_belakang', 'Nama Belakang', 'trim', array());
		$this->form_validation->set_rules('nrp', 'NRP', 'trim|required|numeric|min_length[8]|max_length[8]|callback_check_nrp', array('required' => 'NRP harus diisi', 'numeric' => 'Harus diisi dengan angka.', 'callback_check_nrp' => 'NRP sudah terdaftar'));
		$this->form_validation->set_rules('npwp', 'NPWP', 'trim|numeric|min_length[8]|max_length[15]|callback_check_npwp', array('callback_check_npwp' => 'NPWP sudah terdaftar'));
		$this->form_validation->set_rules('jenis_kelamin', 'Nama Depan', 'trim|required|min_length[1]', array('required' => 'Jenis Kelamin harus diisi.'));
		$this->form_validation->set_rules('tanggal_lahir', 'Nama Depan', 'trim|required|min_length[1]', array('required' => 'Tanggal Lahir harus diisi.'));
		$this->form_validation->set_rules('status_kawin', 'Status Kawin', 'trim|required|min_length[1]', array('required' => 'Status Kawin harus diisi.'));
		$this->form_validation->set_rules('jumlah_anak', 'Jumlah Anak', 'trim|numeric', array('numeric' => 'Harus diisi dengan angka.'));
		$this->form_validation->set_rules('id_pangkat', 'Pangkat', 'trim|required|min_length[1]', array('required' => 'Pangkat harus diisi.'));
		$this->form_validation->set_rules('id_jabatan', 'Jabatan', 'trim|required|min_length[1]', array('required' => 'Jabatan harus diisi.'));
		$this->form_validation->set_rules('masa_kerja', 'Masa Kerja', 'trim|required|numeric|min_length[1]', array('required' => 'Masa Kerja harus diisi.', 'numeric' => 'Harus diisi dengan angka.'));
		if ($this->form_validation->run() == true) {
			$nama_depan         = strtoupper($this->input->post('nama_depan'));
			$nama_belakang      = strtoupper($this->input->post('nama_belakang'));
			$nrp                = $this->input->post('nrp');
			$npwp               = $this->input->post('npwp');
			$jenis_kelamin      = $this->input->post('jenis_kelamin');
			$tmp_tanggal_lahir  = explode('/',trim($this->input->post('tanggal_lahir')));
			//tanggal lahir dipecah jadi array isinya tanggal, bulan, sama tahun
			$tanggal_lahir      = strtotime($tmp_tanggal_lahir['1'].'-'.$tmp_tanggal_lahir['0'].'-'.$tmp_tanggal_lahir['2']);
			//nuker format mm/dd/yy jadi dd/mm/yy
			$status_kawin       = $this->input->post('status_kawin');
			$jumlah_anak        = ($this->input->post('jumlah_anak')) ? $this->input->post('jumlah_anak') : 0;
			//kalo gak ada input jumlah_anak, otomatis jadi 0
			$id_pangkat         = $this->input->post('id_pangkat');
			$id_jabatan         = $this->input->post('id_jabatan');
			$masa_kerja         = $this->input->post('masa_kerja');
			$param = array(
				'nama_depan'    => $nama_depan,
				'nama_belakang' => $nama_belakang,
				'nrp'           => $nrp,
				'npwp'          => $npwp,
				'jenis_kelamin' => $jenis_kelamin,
				'tanggal_lahir' => $tanggal_lahir,
				'status_kawin'  => $status_kawin,
				'jumlah_anak'   => $jumlah_anak,
				'id_pangkat'    => $id_pangkat,
				'id_jabatan'    => $id_jabatan,
				'masa_kerja'    => $masa_kerja,
				'tanggal_lahir' => $tanggal_lahir,
				'status'        => 1,
				'created_by'    => $data['sess']['id'],
				'created_at'    => time(),
				'updated_by'    => $data['sess']['id'],
				'updated_at'    => time()
			);
			if ($result = $this->m_personil->post($param)) {
				$this->session->set_flashdata('msg', "Berhasil.");
				redirect('personil');
			} else {
				$this->session->set_flashdata('msg_g', "Gagal, terjadi kesalahan.");
				redirect($_SERVER['HTTP_REFERER']);
			}
		} else {
			$this->form_validation->set_error_delimiters('', '<br />');
			$this->session->set_flashdata('msg_g', validation_errors());
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function do_edit() {
		$data['sess'] = $this->sess();
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nama_depan', 'Nama Depan', 'trim|required|min_length[1]', array('required' => 'Nama Depan harus diisi.'));
		$this->form_validation->set_rules('nama_belakang', 'Nama Belakang', 'trim', array());
		$this->form_validation->set_rules('nrp', 'NRP', 'trim|required|numeric|min_length[8]|max_length[8]', array('required' => 'NRP harus diisi', 'numeric' => 'Harus diisi dengan angka.'));
		$this->form_validation->set_rules('npwp', 'NPWP', 'trim|numeric|min_length[8]|max_length[15]', array('required' => 'NPWP harus diisi.'));
		$this->form_validation->set_rules('jenis_kelamin', 'Nama Depan', 'trim|required|min_length[1]', array('required' => 'Jenis Kelamin harus diisi.'));
		$this->form_validation->set_rules('tanggal_lahir', 'Nama Depan', 'trim|required|min_length[1]', array('required' => 'Tanggal Lahir harus diisi.'));
		$this->form_validation->set_rules('status_kawin', 'Status Kawin', 'trim|required|min_length[1]', array('required' => 'Status Kawin harus diisi.'));
		$this->form_validation->set_rules('jumlah_anak', 'Jumlah Anak', 'trim|numeric', array('numeric' => 'Harus diisi dengan angka.'));
		$this->form_validation->set_rules('id_pangkat', 'Pangkat', 'trim|required|min_length[1]', array('required' => 'Pangkat harus diisi.'));
		$this->form_validation->set_rules('id_jabatan', 'Jabatan', 'trim|required|min_length[1]', array('required' => 'Jabatan harus diisi.'));
		$this->form_validation->set_rules('masa_kerja', 'Masa Kerja', 'trim|required|numeric|min_length[1]', array('required' => 'Masa Kerja harus diisi.', 'numeric' => 'Harus diisi dengan angka.'));
		$this->form_validation->set_rules('status', 'Status', 'trim|required|min_length[1]', array('required' => 'Status harus diisi.'));
		
		if ($this->form_validation->run() == true) {
			$nama_depan         = strtoupper($this->input->post('nama_depan'));
			$nama_belakang      = strtoupper($this->input->post('nama_belakang'));
			$nrp                = $this->input->post('nrp');
			$npwp               = $this->input->post('npwp');
			$jenis_kelamin      = $this->input->post('jenis_kelamin');
			$tmp_tanggal_lahir  = explode('/',trim($this->input->post('tanggal_lahir')));
			//tanggal lahir dipecah jadi array isinya tanggal, bulan, sama tahun
			$tanggal_lahir      = strtotime($tmp_tanggal_lahir['1'].'-'.$tmp_tanggal_lahir['0'].'-'.$tmp_tanggal_lahir['2']);
			//nuker format mm/dd/yy jadi dd/mm/yy
			$status_kawin       = $this->input->post('status_kawin');
			$jumlah_anak        = ($this->input->post('jumlah_anak')) ? $this->input->post('jumlah_anak') : 0;
			//kalo gak ada input jumlah_anak, otomatis jadi 0
			$id_pangkat         = $this->input->post('id_pangkat');
			$id_jabatan         = $this->input->post('id_jabatan');
			$masa_kerja         = $this->input->post('masa_kerja');
			$status             = $this->input->post('status');
			$param = array(
				'nama_depan'    => $nama_depan,
				'nama_belakang' => $nama_belakang,
				'nrp'           => $nrp,
				'npwp'          => $npwp,
				'jenis_kelamin' => $jenis_kelamin,
				'tanggal_lahir' => $tanggal_lahir,
				'status_kawin'  => $status_kawin,
				'jumlah_anak'   => $jumlah_anak,
				'id_pangkat'    => $id_pangkat,
				'id_jabatan'    => $id_jabatan,
				'masa_kerja'    => $masa_kerja,
				'tanggal_lahir' => $tanggal_lahir,
				'status'        => $status,
				'updated_by'    => $data['sess']['id'],
				'updated_at'    => time()
			);
			if ($result = $this->m_personil->update($nrp, $param)) {
				$this->session->set_flashdata('msg', "Berhasil.");
				redirect('personil');
			} else {
				$this->session->set_flashdata('msg_g', "Gagal, terjadi kesalahan.");
				redirect($_SERVER['HTTP_REFERER']);
			}
		} else {
			$this->form_validation->set_error_delimiters('', '<br />');
			$this->session->set_flashdata('msg_g', validation_errors());
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function check_nrp($nrp) {
		if ($this->m_personil->check('cms_profile', $nrp, 'nrp') == 0) {
			return TRUE;
		} else {
			$this->session->set_flashdata('msg_g', "NRP sudah terdaftar.");
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function check_npwp($npwp) {
		if ($this->m_personil->check('cms_profile', $npwp, 'npwp') == 0) {
			return TRUE;
		} else {
			$this->session->set_flashdata('msg_g', "NPWP sudah terdaftar.");
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
}