<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model(array('m_login', 'm_pembayaran_gaji', 'm_penerima_gaji'));
		if ($sess = $this->session->userdata('user')) {
			//kalo udah login bisa masuk sini
			return TRUE;
		} else {
			//kalo gak login disuruh login dulu
			redirect('login'); 
		}
	}

	function sess() {
		//ambil session
		return $this->session->userdata('user');
	}

	public function index() {
		$data['sess'] = $this->sess();
		$data['latest'] = $this->m_pembayaran_gaji->get_latest('pembayaran_gaji');
		$data['pembayaran_gaji'] = $this->m_pembayaran_gaji->get_all('pembayaran_gaji');
		$data['personil'] = $this->m_penerima_gaji->get_last_personil();
		$this->render->title = 'Penggajian | Laporan Penerima Gaji';
		$this->render->data('laporan/index', $data);
	}

	function do_setujui($id) {
		$data['sess'] = $this->sess();
		$param = array(
			'disetujui' => 1,
			'updated_by' => $data['sess']['id'],
			'updated_at' => time()
		);
		if ($result = $this->m_pembayaran_gaji->update($id, $param)) {
			$this->session->set_flashdata('msg', "Berhasil.");
			redirect('laporan');
		} else {
			$this->session->set_flashdata('msg_g', "Gagal, terjadi kesalahan.");
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
}
