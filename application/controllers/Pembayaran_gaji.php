<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran_gaji extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model(array('m_login', 'm_pembayaran_gaji', 'm_penerima_gaji'));
		if ($this->sess()) {
			//kalo udah login bisa masuk sini
			return TRUE;
		} else {
			//kalo gak login disuruh login dulu
			redirect('login'); 
		}
	}

	function sess() {
		//ambil session
		return $this->session->userdata('user');
	}

	public function index() {
		$data['sess'] = $this->sess();
		$data['pembayaran_gaji'] = $this->m_pembayaran_gaji->get_all('pembayaran_gaji');
		$data['personil'] = $this->m_penerima_gaji->get_last_personil();
		$this->render->title = 'Penggajian | Pembayaran Gaji';
		$this->render->data('pembayaran_gaji/index', $data);
	}

	public function tambah_pembayaran_gaji() {
		if (isset($_POST['do_post'])) {
			$this->do_post();
		} else {
			$data['sess'] = $this->sess();
			if($personil_aktif = $this->m_penerima_gaji->get_all_personil_with_status('aktif')){
				$count = count($personil_aktif);
			} else {
				$count = 0;
			}
			$data['personil_aktif'] = $count;
			$data['personil'] = $this->m_penerima_gaji->get_last_personil();
			$this->render->title = 'Penggajian | Tambah Pembayaran Gaji';
			$this->render->data('pembayaran_gaji/tambah_pembayaran_gaji', $data);
		}
	}

	public function daftar_penerima_gaji($id_pembayaran_gaji) {
		$data['sess'] = $this->sess();
		$data['pembayaran_gaji'] = $this->m_pembayaran_gaji->get($id_pembayaran_gaji);
		$data['penerima_gaji'] = $this->m_penerima_gaji->get_all($data['pembayaran_gaji']['id']);
		$data['personil'] = $this->m_penerima_gaji->get_last_personil();
		$this->render->title = 'Penggajian | Penerima Gaji';
		$this->render->data('pembayaran_gaji/daftar_penerima_gaji', $data);
	}

	public function tambah_penghasilan($id_pembayaran_gaji, $nrp) {
		if (isset($_POST['do_tambah_penghasilan'])) {
			$this->do_tambah_penghasilan();
		} else {
			$data['sess'] = $this->sess();
			$data['pembayaran_gaji'] = $this->m_pembayaran_gaji->get($id_pembayaran_gaji);
			$data['penerima_gaji'] = $this->m_penerima_gaji->get($nrp);
			$data['personil'] = $this->m_penerima_gaji->get_last_personil();
			$this->render->title = 'Penggajian | Tambah Penghasilan';
			$this->render->data('pembayaran_gaji/tambah_penghasilan', $data);
		}
	}

	public function edit_penghasilan($id_pembayaran_gaji, $nrp) {
		if (isset($_POST['do_edit_penghasilan'])) {
			$this->do_edit_penghasilan();
		} else {
			$data['sess'] = $this->sess();
			$data['pembayaran_gaji'] = $this->m_pembayaran_gaji->get($id_pembayaran_gaji);
			$data['penerima_gaji'] = $this->m_penerima_gaji->get_penghasilan($nrp, $id_pembayaran_gaji);
			$data['personil'] = $this->m_penerima_gaji->get_last_personil();
			$this->render->title = 'Penggajian | Edit Penghasilan';
			$this->render->data('pembayaran_gaji/edit_penghasilan', $data);
		}
	}

	public function detail_penghasilan($id_pembayaran_gaji, $nrp) {
		$data['sess'] = $this->sess();
		$data['pembayaran_gaji'] = $this->m_pembayaran_gaji->get($id_pembayaran_gaji);
		$data['penerima_gaji'] = $this->m_penerima_gaji->get_penghasilan($nrp, $id_pembayaran_gaji);
		$data['personil'] = $this->m_penerima_gaji->get_last_personil();
		$this->render->title = 'Penggajian | Detail Penghasilan';
		$this->render->data('pembayaran_gaji/detail_penghasilan', $data);
	}

	function do_post() {
		$data['sess'] = $this->sess();
		$this->load->library('form_validation');
		$this->form_validation->set_rules('kode_bayar_gaji', 'Kode Bayar', 'trim|required|min_length[1]|max_length[10]', array('required' => 'Kode Bayar tidak boleh kosong.'));
		$this->form_validation->set_rules('jumlah_personil', 'Jumlah Personil', 'trim|required|numeric|min_length[1]|max_length[4]', array('required' => 'Jumlah Personil tidak boleh kosong.', 'numeric' => 'Harus diisi dengan angka.'));
		$this->form_validation->set_rules('periode', 'Periode', 'trim|required', array('required' => 'Periode tidak boleh kosong.'));
		if ($this->form_validation->run() == true) {
			$kode_bayar_gaji            = $this->input->post('kode_bayar_gaji');
			$jumlah_personil            = $this->input->post('jumlah_personil');
			$tmp_periode_gaji_dari      = explode('/',trim(substr($this->input->post('periode'), 0, -13)));
			$periode_gaji_dari          = strtotime($tmp_periode_gaji_dari['1'].'-'.$tmp_periode_gaji_dari['0'].'-'.$tmp_periode_gaji_dari['2']);
			$tmp_periode_gaji_sampai    = explode('/',trim(substr($this->input->post('periode'), -10)));
			$periode_gaji_sampai        = strtotime($tmp_periode_gaji_sampai['1'].'-'.$tmp_periode_gaji_sampai['0'].'-'.$tmp_periode_gaji_sampai['2']);
			$param = array(
				'kode_bayar_gaji'       => $kode_bayar_gaji,
				'jumlah_personil'       => $jumlah_personil,
				'total_gaji'            => 0,
				'periode_gaji_dari'     => $periode_gaji_dari,
				'periode_gaji_sampai'   => $periode_gaji_sampai,
				'disetujui'             => 2,
				'slip_gaji_dibuat'      => 2,
				'created_by'            => $data['sess']['id'],
				'created_at'            => time(),
				'updated_by'            => $data['sess']['id'],
				'updated_at'            => time()
			);
			if ($result = $this->m_pembayaran_gaji->post($param)) {
				$tmp    = $this->m_penerima_gaji->get_all_personil_with_status('aktif');
				$size   = count($tmp);
				$p      = 0;
				$myarray = array();
				while($p < $size) {
					$myarray[] = array(
						"profile_id"            => $tmp[$p]['profile_id'],
						"id_pembayaran_gaji"    => $result,
						"dibuat"                => 2,
						'created_by'            => $data['sess']['id'],
						'created_at'            => time(),
						'updated_by'            => $data['sess']['id'],
						'updated_at'            => time()
					);
					$p++;
				}
				if ($result = $this->m_penerima_gaji->post_batch($myarray)) {
					$this->session->set_flashdata('msg', "Berhasil.");
					redirect('pembayaran_gaji');
				}
			} else {
				$this->session->set_flashdata('msg_g', "Gagal, terjadi kesalahan.");
				redirect($_SERVER['HTTP_REFERER']);
			}
		} else {
			$this->form_validation->set_error_delimiters('', '<br />');
			$this->session->set_flashdata('msg_g', validation_errors());
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function kirim_ke_kasikeu($id_pembayaran_gaji) {
		$param = array(
			'disetujui'     => 2,
			'updated_by'    => $data['sess']['id'],
			'updated_at'    => time()
		);
		if ($result = $this->m_pembayaran_gaji->update($id_pembayaran_gaji, $param)) {
			$this->session->set_flashdata('msg', "Berhasil mengirim ke Kasikeu.");
			redirect('pembayaran_gaji');
		} else {
			$this->session->set_flashdata('msg_g', "Gagal, terjadi kesalahan.");
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function do_tambah_penghasilan() {
		$data['sess'] = $this->sess();
		$this->load->library('form_validation');
		$this->form_validation->set_rules('gaji_pokok', 'Gaji Pokok', 'trim|numeric|required|min_length[1]|max_length[10]', array('required' => 'Gaji Pokok tidak boleh kosong.'));
		$this->form_validation->set_rules('tunjangan_suami_istri', 'Tunjangan Suami Istri', 'trim|numeric|required|min_length[1]|max_length[10]', array('required' => 'Tunjangan Suami Istri tidak boleh kosong.'));
		$this->form_validation->set_rules('tunjangan_anak', 'Tunjangan Anak', 'trim|numeric|required|min_length[1]|max_length[10]', array('required' => 'Tunjangan Anak tidak boleh kosong.'));
		$this->form_validation->set_rules('gaji_bruto', 'Gaji Bruto', 'trim|numeric|required|min_length[1]|max_length[10]', array('required' => 'Gaji Bruto tidak boleh kosong.'));
		$this->form_validation->set_rules('tunjangan_lauk_pauk', 'Tunjangan Lauk Pauk', 'trim|numeric|required|min_length[1]|max_length[10]', array('required' => 'Tunjangan Lauk Pauk tidak boleh kosong.'));
		$this->form_validation->set_rules('tunjangan_umum', 'Tunjangan Umum', 'trim|numeric|required|min_length[1]|max_length[10]', array('required' => 'Tunjangan Umum tidak boleh kosong.'));
		$this->form_validation->set_rules('tunjangan_beras', 'Tunjangan Beras', 'trim|numeric|required|min_length[1]|max_length[10]', array('required' => 'Tunjangan Beras tidak boleh kosong.'));
		$this->form_validation->set_rules('tunjangan_profesi', 'Tunjangan Profesi', 'trim|numeric|required|min_length[1]|max_length[10]', array('required' => 'Tunjangan Profesi tidak boleh kosong.'));
		$this->form_validation->set_rules('jumlah_penghasilan_kotor', 'Tunjangan Penghasilan Kotor', 'trim|numeric|required|min_length[1]|max_length[10]', array('required' => 'Jumlah Penghasilan Kotor tidak boleh kosong.'));
		
		$this->form_validation->set_rules('iwp', 'IWP', 'trim|numeric|required|min_length[1]|max_length[10]', array('required' => 'IWP tidak boleh kosong.'));
		$this->form_validation->set_rules('bpjs', 'BPJS', 'trim|numeric|required|min_length[1]|max_length[10]', array('required' => 'BPJS tidak boleh kosong.'));
		$this->form_validation->set_rules('pph_ps_21', 'PPH Ps.21', 'trim|numeric|required|min_length[1]|max_length[10]', array('required' => 'PPH Ps.21 tidak boleh kosong.'));
		$this->form_validation->set_rules('utang', 'Utang', 'trim|numeric|required|min_length[1]|max_length[10]', array('required' => 'Utang tidak boleh kosong.'));
		$this->form_validation->set_rules('jumlah_potongan', 'Tunjangan Potongan', 'trim|numeric|required|min_length[1]|max_length[10]', array('required' => 'Jumlah Potongan tidak boleh kosong.'));
		
		$this->form_validation->set_rules('jumlah_penghasilan_bersih', 'Tunjangan Penghasilan Bersih', 'trim|numeric|required|min_length[1]|max_length[10]', array('required' => 'Jumlah Penghasilan Bersih tidak boleh kosong.'));
		
		if ($this->form_validation->run() == true) {
			$id_pembayaran_gaji         = $this->input->post('id_pembayaran_gaji');
			$nrp                        = $this->input->post('nrp');
			$gaji_pokok                 = $this->input->post('gaji_pokok');
			$tunjangan_suami_istri      = $this->input->post('tunjangan_suami_istri');
			$tunjangan_anak             = $this->input->post('tunjangan_anak');
			$gaji_bruto                 = $this->input->post('gaji_bruto');
			$tunjangan_lauk_pauk        = $this->input->post('tunjangan_lauk_pauk');
			$tunjangan_umum             = $this->input->post('tunjangan_umum');
			$tunjangan_beras            = $this->input->post('tunjangan_beras');
			$tunjangan_profesi          = $this->input->post('tunjangan_profesi');
			$jumlah_penghasilan_kotor   = $this->input->post('jumlah_penghasilan_kotor');
			$iwp                        = $this->input->post('iwp');
			$bpjs                       = $this->input->post('bpjs');
			$pph_ps_21                  = $this->input->post('pph_ps_21');
			$utang                      = $this->input->post('utang');
			$jumlah_potongan            = $this->input->post('jumlah_potongan');
			$jumlah_penghasilan_bersih  = $this->input->post('jumlah_penghasilan_bersih');

			$data_personil = $this->m_penerima_gaji->get($nrp);
			$param = array(
				'profile_id'                => $data_personil['id'],
				'id_pembayaran_gaji'        => $id_pembayaran_gaji,
				'gaji_pokok'                => $gaji_pokok,
				'tunjangan_suami_istri'     => $tunjangan_suami_istri,
				'tunjangan_anak'            => $tunjangan_anak,
				'gaji_bruto'                => $gaji_bruto,
				'tunjangan_lauk_pauk'       => $tunjangan_lauk_pauk,
				'tunjangan_umum'            => $tunjangan_umum,
				'tunjangan_beras'           => $tunjangan_beras,
				'tunjangan_profesi'         => $tunjangan_profesi,
				'jumlah_penghasilan_kotor'  => $jumlah_penghasilan_kotor,
				'iwp'                       => $iwp,
				'bpjs'                      => $bpjs,
				'pph_ps_21'                 => $pph_ps_21,
				'utang'                     => $utang,
				'jumlah_potongan'           => $jumlah_potongan,
				'jumlah_penghasilan_kotor'  => $jumlah_penghasilan_kotor,
				'jumlah_penghasilan_bersih' => $jumlah_penghasilan_bersih,
				'dibuat'                    => 1,
				'created_by'                => $data['sess']['id'],
				'created_at'                => time(),
				'updated_by'                => $data['sess']['id'],
				'updated_at'                => time()
			);
			if ($result = $this->m_penerima_gaji->post($data_personil['id'], $id_pembayaran_gaji, $param)) {
				if($update_jumlah_gaji = $this->m_pembayaran_gaji->update_jumlah_gaji($id_pembayaran_gaji)) {
					$this->session->set_flashdata('msg', "Berhasil.");
					redirect('pembayaran_gaji/daftar_penerima_gaji/' . $id_pembayaran_gaji);
				}
				$this->session->set_flashdata('msg_g', "Gagal, terjadi kesalahan.");
				redirect($_SERVER['HTTP_REFERER']);
			} else {
				$this->session->set_flashdata('msg_g', "Gagal, terjadi kesalahan.");
				redirect($_SERVER['HTTP_REFERER']);
			}
		} else {
			$this->form_validation->set_error_delimiters('', '<br />');
			$this->session->set_flashdata('msg_g', validation_errors());
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function do_edit_penghasilan() {
		$data['sess'] = $this->sess();
		$this->load->library('form_validation');
		$this->form_validation->set_rules('gaji_pokok', 'Gaji Pokok', 'trim|numeric|required|min_length[1]|max_length[10]', array('required' => 'Gaji Pokok tidak boleh kosong.'));
		$this->form_validation->set_rules('tunjangan_suami_istri', 'Tunjangan Suami Istri', 'trim|numeric|required|min_length[1]|max_length[10]', array('required' => 'Tunjangan Suami Istri tidak boleh kosong.'));
		$this->form_validation->set_rules('tunjangan_anak', 'Tunjangan Anak', 'trim|numeric|required|min_length[1]|max_length[10]', array('required' => 'Tunjangan Anak tidak boleh kosong.'));
		$this->form_validation->set_rules('gaji_bruto', 'Gaji Bruto', 'trim|numeric|required|min_length[1]|max_length[10]', array('required' => 'Gaji Bruto tidak boleh kosong.'));
		$this->form_validation->set_rules('tunjangan_lauk_pauk', 'Tunjangan Lauk Pauk', 'trim|numeric|required|min_length[1]|max_length[10]', array('required' => 'Tunjangan Lauk Pauk tidak boleh kosong.'));
		$this->form_validation->set_rules('tunjangan_umum', 'Tunjangan Umum', 'trim|numeric|required|min_length[1]|max_length[10]', array('required' => 'Tunjangan Umum tidak boleh kosong.'));
		$this->form_validation->set_rules('tunjangan_beras', 'Tunjangan Beras', 'trim|numeric|required|min_length[1]|max_length[10]', array('required' => 'Tunjangan Beras tidak boleh kosong.'));
		$this->form_validation->set_rules('tunjangan_profesi', 'Tunjangan Profesi', 'trim|numeric|required|min_length[1]|max_length[10]', array('required' => 'Tunjangan Profesi tidak boleh kosong.'));
		$this->form_validation->set_rules('jumlah_penghasilan_kotor', 'Tunjangan Penghasilan Kotor', 'trim|numeric|required|min_length[1]|max_length[10]', array('required' => 'Jumlah Penghasilan Kotor tidak boleh kosong.'));
		
		$this->form_validation->set_rules('iwp', 'IWP', 'trim|numeric|required|min_length[1]|max_length[10]', array('required' => 'IWP tidak boleh kosong.'));
		$this->form_validation->set_rules('bpjs', 'BPJS', 'trim|numeric|required|min_length[1]|max_length[10]', array('required' => 'BPJS tidak boleh kosong.'));
		$this->form_validation->set_rules('pph_ps_21', 'PPH Ps.21', 'trim|numeric|required|min_length[1]|max_length[10]', array('required' => 'PPH Ps.21 tidak boleh kosong.'));
		$this->form_validation->set_rules('utang', 'Utang', 'trim|numeric|required|min_length[1]|max_length[10]', array('required' => 'Utang tidak boleh kosong.'));
		$this->form_validation->set_rules('jumlah_potongan', 'Tunjangan Potongan', 'trim|numeric|required|min_length[1]|max_length[10]', array('required' => 'Jumlah Potongan tidak boleh kosong.'));
		
		$this->form_validation->set_rules('jumlah_penghasilan_bersih', 'Tunjangan Penghasilan Bersih', 'trim|numeric|required|min_length[1]|max_length[10]', array('required' => 'Jumlah Penghasilan Bersih tidak boleh kosong.'));
		
		if ($this->form_validation->run() == true) {
			$id_pembayaran_gaji = $this->input->post('id_pembayaran_gaji');
			$nrp = $this->input->post('nrp');
			$gaji_pokok = $this->input->post('gaji_pokok');
			$tunjangan_suami_istri = $this->input->post('tunjangan_suami_istri');
			$tunjangan_anak = $this->input->post('tunjangan_anak');
			$gaji_bruto = $this->input->post('gaji_bruto');
			$tunjangan_lauk_pauk = $this->input->post('tunjangan_lauk_pauk');
			$tunjangan_umum = $this->input->post('tunjangan_umum');
			$tunjangan_beras = $this->input->post('tunjangan_beras');
			$tunjangan_profesi = $this->input->post('tunjangan_profesi');
			$jumlah_penghasilan_kotor = $this->input->post('jumlah_penghasilan_kotor');
			$iwp = $this->input->post('iwp');
			$bpjs = $this->input->post('bpjs');
			$pph_ps_21 = $this->input->post('pph_ps_21');
			$utang = $this->input->post('utang');
			$jumlah_potongan = $this->input->post('jumlah_potongan');
			$jumlah_penghasilan_bersih = $this->input->post('jumlah_penghasilan_bersih');

			$data_personil = $this->m_penerima_gaji->get($nrp);
			$param = array(
				'profile_id' => $data_personil['id'],
				'id_pembayaran_gaji' => $id_pembayaran_gaji,
				'gaji_pokok' => $gaji_pokok,
				'tunjangan_suami_istri' => $tunjangan_suami_istri,
				'tunjangan_anak' => $tunjangan_anak,
				'gaji_bruto' => $gaji_bruto,
				'tunjangan_lauk_pauk' => $tunjangan_lauk_pauk,
				'tunjangan_umum' => $tunjangan_umum,
				'tunjangan_beras' => $tunjangan_beras,
				'tunjangan_profesi' => $tunjangan_profesi,
				'jumlah_penghasilan_kotor' => $jumlah_penghasilan_kotor,
				'iwp' => $iwp,
				'bpjs' => $bpjs,
				'pph_ps_21' => $pph_ps_21,
				'utang' => $utang,
				'jumlah_potongan' => $jumlah_potongan,
				'jumlah_penghasilan_kotor' => $jumlah_penghasilan_kotor,
				'jumlah_penghasilan_bersih' => $jumlah_penghasilan_bersih,
				'updated_by' => $data['sess']['id'],
				'updated_at' => time()
			);
			if ($result = $this->m_penerima_gaji->post($data_personil['id'], $id_pembayaran_gaji, $param)) {
				if($update_jumlah_gaji = $this->m_pembayaran_gaji->update_jumlah_gaji($id_pembayaran_gaji)) {
					$this->session->set_flashdata('msg', "Berhasil.");
					redirect('pembayaran_gaji/daftar_penerima_gaji/' . $id_pembayaran_gaji);
				}
				$this->session->set_flashdata('msg_g', "Gagal, terjadi kesalahan.");
				redirect($_SERVER['HTTP_REFERER']);
			} else {
				$this->session->set_flashdata('msg_g', "Gagal, terjadi kesalahan.");
				redirect($_SERVER['HTTP_REFERER']);
			}
		} else {
			$this->form_validation->set_error_delimiters('', '<br />');
			$this->session->set_flashdata('msg_g', validation_errors());
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
}
