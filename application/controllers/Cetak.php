<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(THIRDPARTY . 'phpoffice' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

setlocale(LC_ALL, array('IND', 'id_ID'));
class Cetak extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model(array('m_penerima_gaji', 'm_pembayaran_gaji'));
	}   

	function index() {}

	function slip_gaji($kode_bayar_gaji) {
		$result = $this->m_pembayaran_gaji->get_slip_gaji($kode_bayar_gaji);

		$data['sess'] = $this->session->userdata('user');
		$param = array(
			'slip_gaji_dibuat'  => 1,
			'updated_by'        => $data['sess']['id'],
			'updated_at'        => time()
		);
		$result = $this->m_pembayaran_gaji->update($result['id'], $param);
		if ($result) {
			$this->download_pdf($kode_bayar_gaji);
		}
	}

	function download_pdf($kode_bayar_gaji) {
		$this->load->library('pdf');
		$data['sess'] = $this->session->userdata('user');
		$data['slip_gaji'] = $this->m_penerima_gaji->get_slip_gaji($kode_bayar_gaji);
		$this->load->view('cetak/slip_gaji', $data);
	}

	function laporan_pembayaran_gaji($kode_bayar_gaji) {
		$sess = $this->session->userdata('user');
		$periode = $this->m_penerima_gaji->get_periode($kode_bayar_gaji);
		$pembayaran_gaji = $this->m_penerima_gaji->get_laporan($kode_bayar_gaji);

		$kolom = 6;
		$last = count($pembayaran_gaji) + $kolom;
		$a_last = 'A' . $last;
		$j_last = 'J' . $last;

		$spreadsheet = new Spreadsheet;
		$sheet = $spreadsheet->getActiveSheet();

		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A2', 'NO.')
			->setCellValue('B2', 'NAMA')
			->setCellValue('C2', 'TANGGAL LAHIR')
			->setCellValue('D2', 'NRP')
			->setCellValue('E2', 'JABATAN, PANGKAT')
			->setCellValue('F2', 'NPWP')
			->setCellValue('G2', 'STS KAWIN')
			->setCellValue('H2', 'JML JIWA')
			->setCellValue('I2', 'KDGAPOK')
			->setCellValue('J2', 'TMT MKG')
			->setCellValue('K2', 'JUMLAH PENGHASILAN KOTOR')
			->setCellValue('T2', 'POTONGAN')
			->setCellValue('Y2', 'JML BERSIH YANG DIBAYARKAN')
			->setCellValue('K3', 'GAJI POKOK')
			->setCellValue('L3', 'TUNJANGAN SUAMI/ISTRI')
			->setCellValue('M3', 'TUNJANGAN ANAK')
			->setCellValue('N3', 'GAJI BRUTO')
			->setCellValue('O3', 'TUNJANGAN LAUK PAUK')
			->setCellValue('P3', 'TUNJANGAN UMUM')
			->setCellValue('Q3', 'TUNJANGAN BERAS')
			->setCellValue('R3', 'TUNJANGAN POLMAS / BHABINKAMTIBMAS')
			->setCellValue('S3', 'JUMLAH PENGHASILAN KOTOR')
			->setCellValue('T3', 'IWP')
			->setCellValue('U3', 'BPJS')
			->setCellValue('V3', 'PPS Ps. 21')
			->setCellValue('W3', 'UTANG')
			->setCellValue('X3', 'JML POTONGAN')
			->setCellValue('A' . $last, 'TOTAL');

		$sheet->mergeCells('A2:A5')->getColumnDimension('A')->setAutoSize(true);
		$sheet->mergeCells('B2:B5')->getColumnDimension('B')->setAutoSize(true);
		$sheet->mergeCells('C2:C5')->getColumnDimension('C')->setAutoSize(true);
		$sheet->mergeCells('D2:D5')->getColumnDimension('D')->setAutoSize(true);
		$sheet->mergeCells('E2:E5')->getColumnDimension('E')->setAutoSize(true);
		$sheet->mergeCells('F2:F5')->getColumnDimension('F')->setAutoSize(true);
		$sheet->mergeCells('G2:G5')->getColumnDimension('G')->setAutoSize(true);
		$sheet->mergeCells('H2:H5')->getColumnDimension('H')->setAutoSize(true);
		$sheet->mergeCells('I2:I5')->getColumnDimension('I')->setAutoSize(true);
		$sheet->mergeCells('J2:J5')->getColumnDimension('J')->setAutoSize(true);
		$sheet->mergeCells('K2:S2')->getColumnDimension('K')->setAutoSize(true);
		$sheet->mergeCells('T2:X2')->getColumnDimension('T')->setAutoSize(true);
		$sheet->mergeCells('Y2:Y5')->getColumnDimension('Y')->setAutoSize(true);
		$sheet->mergeCells('K3:K5')->getColumnDimension('K')->setAutoSize(true);
		$sheet->mergeCells('L3:L5')->getColumnDimension('L')->setAutoSize(true);
		$sheet->mergeCells('M3:M5')->getColumnDimension('M')->setAutoSize(true);
		$sheet->mergeCells('N3:N5')->getColumnDimension('N')->setAutoSize(true);
		$sheet->mergeCells('O3:O5')->getColumnDimension('O')->setAutoSize(true);
		$sheet->mergeCells('P3:P5')->getColumnDimension('P')->setAutoSize(true);
		$sheet->mergeCells('Q3:Q5')->getColumnDimension('Q')->setAutoSize(true);
		$sheet->mergeCells('R3:R5')->getColumnDimension('R')->setAutoSize(true);
		$sheet->mergeCells('S3:S5')->getColumnDimension('S')->setAutoSize(true);
		$sheet->mergeCells('T3:T5')->getColumnDimension('T')->setAutoSize(true);
		$sheet->mergeCells('U3:U5')->getColumnDimension('Y')->setAutoSize(true);
		$sheet->mergeCells('V3:V5')->getColumnDimension('V')->setAutoSize(true);
		$sheet->mergeCells('W3:W5')->getColumnDimension('W')->setAutoSize(true);
		$sheet->mergeCells('X3:X5')->getColumnDimension('X')->setAutoSize(true);
		$sheet->mergeCells("$a_last:$j_last")->getColumnDimension('A')->setAutoSize(true);

		$spreadsheet->getDefaultStyle()->getFont()->setSize(10);

		$styleArray = [
			'borders' => [
				'allBorders' => [
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					'color' => ['argb' => '00000000'],
				],
			],
			'alignment' => [
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
			]
		];
		$sheet->getStyle('A2:Y' . $last)->applyFromArray($styleArray);

		$styleArray_bold = [
			'font' => [
				'name' => 'Arial',
				'bold' => true,
			]
		];
		$sheet->getStyle('A2:Y' . ($kolom - 1))->applyFromArray($styleArray_bold);
		$sheet->getStyle('A' . $last . ':Y' . $last)->applyFromArray($styleArray_bold);

		$styleArray_number = [
			'numberFormat' => [
				'formatCode' => \PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER
			]
		];
		$sheet->getStyle('F6:F' . ($last - 1))->applyFromArray($styleArray_number);

		$styleArray_comma = [
			'numberFormat' => [
				'formatCode' => \PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
			]
		];
		$sheet->getStyle('K6:Y' . ($last))->applyFromArray($styleArray_comma);

		foreach($pembayaran_gaji as $key => $res) {
			if ($res['status_kawin'] == 0) {
				$status_kawin = 'TK';
				$jumlah_jiwa_tmp = 110;
			} else {
				$status_kawin = 'K';
				if ($res['jenis_kelamin'] == 0) {
					$jumlah_jiwa_tmp = 010;
				} else {
					$jumlah_jiwa_tmp = 100;
				}
			}
			$jumlah_jiwa = $jumlah_jiwa_tmp . '' . $res['jumlah_anak'];
			if ($res['nama_pangkat'] == 'AIPTU') {
				$kode_gapok = '2F';
			}

			$spreadsheet->setActiveSheetIndex(0)
				->setCellValue('A' . $kolom, $key+1)
				->setCellValue('B' . $kolom, $res['nama_depan'] . ' ' . $res['nama_belakang'])
				->setCellValue('C' . $kolom, date('d F Y', $res['tanggal_lahir']))
				->setCellValue('D' . $kolom, $res['nrp'])
				->setCellValue('E' . $kolom, $res['nama_jabatan'] . '/' . $res['nama_pangkat'])
				->setCellValue('F' . $kolom, $res['npwp'])//format cell YANG INI BELOMAN 
				->setCellValue('G' . $kolom, $status_kawin)
				->setCellValue('H' . $kolom, $jumlah_jiwa)
				->setCellValue('I' . $kolom, $kode_gapok)
				->setCellValue('J' . $kolom, $res['masa_kerja'])
				->setCellValue('K' . $kolom, $res['gaji_pokok'])
				->setCellValue('L' . $kolom, $res['tunjangan_suami_istri'])
				->setCellValue('M' . $kolom, $res['tunjangan_anak'])
				->setCellValue('N' . $kolom, $res['gaji_bruto'])
				->setCellValue('O' . $kolom, $res['tunjangan_lauk_pauk'])
				->setCellValue('P' . $kolom, $res['tunjangan_umum'])
				->setCellValue('Q' . $kolom, $res['tunjangan_beras'])
				->setCellValue('R' . $kolom, $res['tunjangan_profesi'])
				->setCellValue('S' . $kolom, $res['jumlah_penghasilan_kotor'])
				->setCellValue('T' . $kolom, $res['iwp'])
				->setCellValue('U' . $kolom, $res['bpjs'])
				->setCellValue('V' . $kolom, $res['pph_ps_21'])
				->setCellValue('W' . $kolom, $res['utang'])
				->setCellValue('X' . $kolom, $res['jumlah_potongan'])
				->setCellValue('Y' . $kolom, $res['jumlah_penghasilan_bersih']);
			$kolom++;
		}

		$formula_gaji_pokok                 = '=SUM(K6:K'.($last-1).')';
		$sheet->getCell('K' . $last)->setValue($formula_gaji_pokok);
		$formula_tunjangan_suami_istri      = '=SUM(L6:L'.($last-1).')';
		$sheet->getCell('L' . $last)->setValue($formula_tunjangan_suami_istri);
		$formula_tunjangan_anak             = '=SUM(M6:M'.($last-1).')';
		$sheet->getCell('M' . $last)->setValue($formula_tunjangan_anak);
		$formula_gaji_bruto                 = '=SUM(N6:N'.($last-1).')';
		$sheet->getCell('N' . $last)->setValue($formula_gaji_bruto);
		$formula_tunjangan_lauk_pauk        = '=SUM(O6:O'.($last-1).')';
		$sheet->getCell('O' . $last)->setValue($formula_tunjangan_lauk_pauk);
		$formula_tunjangan_umum             = '=SUM(P6:P'.($last-1).')';
		$sheet->getCell('P' . $last)->setValue($formula_tunjangan_umum);
		$formula_tunjangan_beras            = '=SUM(Q6:Q'.($last-1).')';
		$sheet->getCell('Q' . $last)->setValue($formula_tunjangan_beras);
		$formula_tunjangan_profesi          = '=SUM(R6:R'.($last-1).')';
		$sheet->getCell('R' . $last)->setValue($formula_tunjangan_profesi);
		$formula_jumlah_penghasilan_kotor   = '=SUM(S6:S'.($last-1).')';
		$sheet->getCell('S' . $last)->setValue($formula_jumlah_penghasilan_kotor);
		$formula_iwp                        = '=SUM(T6:T'.($last-1).')';
		$sheet->getCell('T' . $last)->setValue($formula_iwp);
		$formula_bpjs                       = '=SUM(U6:U'.($last-1).')';
		$sheet->getCell('U' . $last)->setValue($formula_bpjs);
		$formula_pph_ps_21                  = '=SUM(V6:V'.($last-1).')';
		$sheet->getCell('V' . $last)->setValue($formula_pph_ps_21);
		$formula_utang                      = '=SUM(W6:W'.($last-1).')';
		$sheet->getCell('W' . $last)->setValue($formula_utang);
		$formula_jumlah_potongan            = '=SUM(X6:X'.($last-1).')';
		$sheet->getCell('X' . $last)->setValue($formula_jumlah_potongan);
		$formula_jumlah_penghasilan_bersih  = '=SUM(Y6:Y'.($last-1).')';
		$sheet->getCell('Y' . $last)->setValue($formula_jumlah_penghasilan_bersih);
		$writer = new Xlsx($spreadsheet);

		$file_name = 'LAPORAN_PEMBAYARAN_GAJI_' . date("M_Y", $periode['periode_gaji_dari']);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="' . $file_name . '.xlsx"');
		header('Cache-Control: max-age=0');   
		$writer->save('php://output');
	}
}
