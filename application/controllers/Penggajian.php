<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penggajian extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('m_login', 'm_pembayaran_gaji', 'm_penerima_gaji'));
        if ($sess = $this->session->userdata('user')) {
            //kalo udah login bisa masuk sini
            return TRUE;
        } else {
            //kalo gak login disuruh login dulu
            redirect('login'); 
        }
    }

    public function index() {
        $data['sess'] = $this->session->userdata('user');
        if($data['sess']['username'] !== 'kasium') {
            $data['latest'] = $this->m_pembayaran_gaji->get_latest('pembayaran_gaji');
        }
        $data['personil'] = $this->m_penerima_gaji->get_last_personil();

        $personil_aktif = $this->m_penerima_gaji->get_all_personil_with_status('aktif');
		if (empty($personil_aktif)) {
            $data['personil_aktif'] = 0;
		} else {
            $data['personil_aktif'] = count($personil_aktif);
        }

        $personil_nonaktif = $this->m_penerima_gaji->get_all_personil_with_status('nonaktif');
		if (empty($personil_nonaktif)) {
            $data['personil_nonaktif'] = 0;
		} else {
            $data['personil_nonaktif'] = count($personil_nonaktif);
        }

        $total_personil = $this->m_penerima_gaji->get_all_personil();
		if (empty($total_personil)) {
            $data['total_personil'] = 0;
        } else {
            $data['total_personil'] = count($total_personil);
        }

        $this->render->title = 'Penggajian | Beranda';
        $this->render->data('index', $data);
    }
}
