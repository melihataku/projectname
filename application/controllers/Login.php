<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_login');
    }

    public function index() {
        if (isset($_POST['do_login'])) {
            $this->do_login();
        } else {
            $this->render->title = 'Penggajian | Halaman Login';
            $this->render->layout = 'main_login';
            $this->render->data('login');
        }
    }

    function do_login() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nrp', 'nrp', 'numeric|trim|required', array('required' => 'NRP tidak boleh kosong.'));
        $this->form_validation->set_rules('password', 'password', 'required', array('required' => 'Password tidak boleh kosong.'));
        if ($this->form_validation->run() == true) {
            $nrp         = $this->input->post('nrp');
            $password    = $this->input->post('password');
            if ($login = $this->m_login->login($nrp, $password)) {
                if (!isset($data['data_session'])) {
                    $data['data_session'] = array();
                }
                $data['data_session'] = array(
                    'id'            => $login['id'],
                    'username'      => $login['username'],
                    'nrp'           => $login['nrp'],
                    'nama_depan'    => $login['nama_depan'],
                    'nama_belakang' => $login['nama_belakang'],
                    'jenis_kelamin' => $login['jenis_kelamin'],
                    'nama_pangkat'  => $login['nama_pangkat'],
                    'nama_jabatan'  => $login['nama_jabatan'],
                    'created_at'    => $login['created_at']
                );
                $this->session->set_userdata('user', $data['data_session']);
                redirect('penggajian');
            } else {
                $this->session->set_flashdata('msg_g', 'Username dan password tidak cocok');
                redirect('login');
            }
        } else {
            $this->form_validation->set_error_delimiters('', '<br />');
            $this->session->set_flashdata('msg_g', validation_errors());
            redirect('login');
        }
    }
}
