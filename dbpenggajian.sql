-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 06, 2020 at 03:30 AM
-- Server version: 5.7.24
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbpenggajian`
--

-- --------------------------------------------------------

--
-- Table structure for table `cms_profile`
--

CREATE TABLE `cms_profile` (
  `id` int(4) UNSIGNED NOT NULL,
  `nrp` int(8) NOT NULL,
  `npwp` bigint(15) NOT NULL,
  `tanggal_lahir` int(11) NOT NULL,
  `nama_depan` varchar(15) NOT NULL,
  `nama_belakang` varchar(45) DEFAULT NULL,
  `id_pangkat` int(4) UNSIGNED NOT NULL,
  `id_jabatan` int(4) UNSIGNED NOT NULL,
  `jenis_kelamin` int(1) NOT NULL COMMENT '1 berarti laki-laki, 0 berarti perempuan',
  `status_kawin` int(1) NOT NULL COMMENT '1 berarti kawin, 0 berarti belum kawin',
  `jumlah_anak` int(1) NOT NULL,
  `masa_kerja` int(3) NOT NULL,
  `status` int(1) NOT NULL COMMENT '1 berarti aktif, 2 berarti tidak aktif',
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_profile`
--

INSERT INTO `cms_profile` (`id`, `nrp`, `npwp`, `tanggal_lahir`, `nama_depan`, `nama_belakang`, `id_pangkat`, `id_jabatan`, `jenis_kelamin`, `status_kawin`, `jumlah_anak`, `masa_kerja`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 74010245, 748127412874128, 873106475, 'ANDRIZA', 'YUNI', 1, 1, 0, 0, 0, 35, 2, 1592452107, 1, 1592452107, 1),
(2, 74010255, 371283713821732, 875180075, 'YENNY', 'SANTI MARIA', 1, 2, 0, 0, 0, 35, 2, 1592452107, 1, 1592452107, 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
(6);

-- --------------------------------------------------------

--
-- Table structure for table `ms_jabatan`
--

CREATE TABLE `ms_jabatan` (
  `id` int(4) UNSIGNED NOT NULL,
  `nama_jabatan` varchar(100) NOT NULL,
  `created_by` int(4) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_by` int(4) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ms_jabatan`
--

INSERT INTO `ms_jabatan` (`id`, `nama_jabatan`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 'Kasium', 1, 1592452107, 1, 1592452107),
(2, 'Kasikeu', 1, 1592452107, 1, 1592452107),
(3, 'Bhabinkamtibmas', 1, 1592452107, 1, 1592452107);

-- --------------------------------------------------------

--
-- Table structure for table `ms_pangkat`
--

CREATE TABLE `ms_pangkat` (
  `id` int(4) UNSIGNED NOT NULL,
  `nama_pangkat` varchar(30) NOT NULL,
  `created_by` int(4) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_by` int(4) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ms_pangkat`
--

INSERT INTO `ms_pangkat` (`id`, `nama_pangkat`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 'AIPTU', 1, 1592452107, 1, 1592452107),
(2, 'IPTU', 1, 1592452107, 1, 1592452107);

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_gaji`
--

CREATE TABLE `pembayaran_gaji` (
  `id` int(4) UNSIGNED NOT NULL,
  `kode_bayar_gaji` varchar(20) NOT NULL,
  `periode_gaji_dari` int(11) DEFAULT NULL,
  `periode_gaji_sampai` int(11) DEFAULT NULL,
  `jumlah_personil` int(5) DEFAULT NULL,
  `total_gaji` bigint(20) DEFAULT NULL,
  `disetujui` int(1) NOT NULL COMMENT 'disetujui oleh kasikeu, 1 berarti sudah, 2 berarti belum',
  `slip_gaji_dibuat` int(1) NOT NULL COMMENT 'membuat slip gaji setelah disetujui kasikeu, 1 berarti sudah, 2 berarti belum',
  `created_by` int(4) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_by` int(4) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `penerima_gaji`
--

CREATE TABLE `penerima_gaji` (
  `id` int(11) UNSIGNED NOT NULL,
  `profile_id` int(4) UNSIGNED NOT NULL,
  `id_pembayaran_gaji` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `gaji_pokok` int(9) NOT NULL DEFAULT '0',
  `tunjangan_suami_istri` int(9) NOT NULL DEFAULT '0',
  `tunjangan_anak` int(9) NOT NULL DEFAULT '0',
  `gaji_bruto` int(9) NOT NULL COMMENT 'gaji_pokok+tunjangan_suami_istri+tunjangan_anak',
  `tunjangan_lauk_pauk` int(7) NOT NULL DEFAULT '0',
  `tunjangan_umum` int(5) NOT NULL DEFAULT '0',
  `tunjangan_beras` int(9) NOT NULL DEFAULT '0',
  `tunjangan_profesi` int(9) NOT NULL DEFAULT '0',
  `jumlah_penghasilan_kotor` int(9) NOT NULL COMMENT 'gaji_bruto+semua_tunjangan',
  `iwp` int(9) NOT NULL DEFAULT '0',
  `bpjs` int(9) NOT NULL DEFAULT '0',
  `pph_ps_21` int(9) NOT NULL DEFAULT '0',
  `utang` int(9) NOT NULL DEFAULT '0',
  `jumlah_potongan` int(9) NOT NULL DEFAULT '0',
  `jumlah_penghasilan_bersih` int(9) NOT NULL COMMENT 'jumlah_penghasilan_kotor-jumlah_potongan',
  `dibuat` int(1) DEFAULT NULL COMMENT 'membuat daftar penerima gaji oleh kasium sebelum ditandatangan, 1 berarti sudah ditambahkan datanya',
  `created_by` int(4) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_by` int(4) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) UNSIGNED NOT NULL,
  `profile_id` int(4) UNSIGNED NOT NULL,
  `username` varchar(10) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `profile_id`, `username`, `password_hash`, `created_at`, `updated_at`) VALUES
(1, 1, 'kasium', '$2y$10$gytvU3IW.WsnWroXjYvGm.MdCZEPYijdQg48Qdka567QAgoF6xlme', 1592452107, 1592452107),
(2, 2, 'kasikeu', '$2y$10$gytvU3IW.WsnWroXjYvGm.MdCZEPYijdQg48Qdka567QAgoF6xlme', 1592452107, 1592452107);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cms_profile`
--
ALTER TABLE `cms_profile`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-cms_profile-id_pangkat` (`id_pangkat`),
  ADD KEY `idx-cms_profile-id_jabatan` (`id_jabatan`);

--
-- Indexes for table `ms_jabatan`
--
ALTER TABLE `ms_jabatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_pangkat`
--
ALTER TABLE `ms_pangkat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembayaran_gaji`
--
ALTER TABLE `pembayaran_gaji`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penerima_gaji`
--
ALTER TABLE `penerima_gaji`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-penerima_gaji-profile_id` (`profile_id`),
  ADD KEY `idx-penerima_gaji-id_pembayaran_gaji` (`id_pembayaran_gaji`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-user-profile_id` (`profile_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cms_profile`
--
ALTER TABLE `cms_profile`
  MODIFY `id` int(4) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ms_jabatan`
--
ALTER TABLE `ms_jabatan`
  MODIFY `id` int(4) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ms_pangkat`
--
ALTER TABLE `ms_pangkat`
  MODIFY `id` int(4) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pembayaran_gaji`
--
ALTER TABLE `pembayaran_gaji`
  MODIFY `id` int(4) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `penerima_gaji`
--
ALTER TABLE `penerima_gaji`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cms_profile`
--
ALTER TABLE `cms_profile`
  ADD CONSTRAINT `fk-cms_profile-id_jabatan` FOREIGN KEY (`id_jabatan`) REFERENCES `ms_jabatan` (`id`),
  ADD CONSTRAINT `fk-cms_profile-id_pangkat` FOREIGN KEY (`id_pangkat`) REFERENCES `ms_pangkat` (`id`);

--
-- Constraints for table `penerima_gaji`
--
ALTER TABLE `penerima_gaji`
  ADD CONSTRAINT `fk-penerima_gaji-id_pembayaran_gaji` FOREIGN KEY (`id_pembayaran_gaji`) REFERENCES `pembayaran_gaji` (`id`),
  ADD CONSTRAINT `fk-penerima_gaji-profile_id` FOREIGN KEY (`profile_id`) REFERENCES `cms_profile` (`id`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk-user-profile_id` FOREIGN KEY (`profile_id`) REFERENCES `cms_profile` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
